import { ContainerService } from './container.service';
import { LayoutModule } from './layout/layout.module';
import { AuthService } from './providers/auth.service';
import { TopicModule } from './topic/topic.module';
import { ContinuePipe } from './pipes/continue.pipe';
import { AppReducers } from './store/app.reducers';
import { SharedService } from './providers/shared.service';
import { AuthInterceptor } from './providers/interceptors/auth-interceptor.service';
import { PostModule } from './post/post.module';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgxSpinnerModule } from 'ngx-spinner';
import { AppComponent } from './app.component';
import { Routing } from './app.routing';
import { EventsService } from './providers/events.service';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { PostEffects } from './post/store/post.effects';
import { MainEffects } from './layout/store/main.effects';
import { ScriptsComponent } from './scripts/scripts.component';
import { TokenGuard } from './providers/token.guard';

@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    NgxSpinnerModule,
    Routing,
    PostModule,
    TopicModule,
    StoreModule.forRoot(AppReducers),
    EffectsModule.forRoot([MainEffects, PostEffects]),
    LayoutModule],
  declarations: [
    AppComponent,
    ContinuePipe,
    ScriptsComponent
  ],
  providers: [
    SharedService,
    TokenGuard,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true,
    },
    EventsService,
    ContainerService,
    AuthService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
