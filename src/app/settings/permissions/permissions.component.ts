import { AppState } from './../../store/app.reducers';
import { Component, OnInit } from '@angular/core';
import { Store } from '../../../../node_modules/@ngrx/store';

@Component({
  selector: 'app-permissions',
  templateUrl: './permissions.component.html',
  styleUrls: ['./permissions.component.css']
})
export class PermissionsComponent implements OnInit {

  constructor(private store: Store<AppState>) { }
  template;
  project;
  ngOnInit() {
    this.store.select('main').subscribe(state => {
      this.template = state.template;
      this.project = state.project;
      this.fillPermissions();
      console.log(this.template);
    });
  }

  fillPermissions() {
    this.template.roles.forEach(r => {
      r.permissions.forEach(p => {
        console.log(p, 'p')
        var permissions = [];
        // if (this.project.my_permissions.indexOf(p) !== -1) {
        //   permissions.push({
        //     [p]: {
        //       name: p,
        //       isPermitted:  
        //     }
        //   })
        // }
        // else {
        //   permissions.push({
        //     [p]: {
        //       name: p,
        //       isPermitted: false
        //     }
        //   })
        // }
        r.permissions = permissions;
      })
    })

  }
}