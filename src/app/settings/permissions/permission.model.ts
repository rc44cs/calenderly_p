export class PermissionModal {
    computable: boolean;
    name: string;
    order: number;
    permissions = [];
    slug: string;
}
