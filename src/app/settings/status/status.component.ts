import { Component, OnInit } from '@angular/core';
import { Store } from 'node_modules/@ngrx/store';
import { AppState } from '../../store/app.reducers';

@Component({
  selector: 'app-status',
  templateUrl: './status.component.html',
  styleUrls: ['./status.component.css']
})
export class StatusComponent implements OnInit {

  constructor(private store: Store<AppState>) { }
  project: any;
  ngOnInit() {
    this.store.select('main').subscribe(state => {
      this.project = state.template;
    })
  }

}
