import { environment } from '../../../environments/environment';
import { of, Subject, Observable, forkJoin, empty } from 'rxjs';
import { Store, State } from '@ngrx/store';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AppState } from '../../store/app.reducers';
import * as _ from 'underscore';
import 'rxjs/Rx';
import { catchError, mergeMap, map, switchMap } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class SettingService {
    posts: any[] = [];

    constructor(
        private http: HttpClient,
        private router: Router,
        private store: Store<AppState>,
        private state: State<AppState>
    ) {
    }

    // public changeOwner(projectId, userId): Observable<any> {
    //     const obj = {
    //         user: userId
    //     };
    //     const url = (environment.url + '/api/v1/projects/' + projectId + '/transfer_start');
    //     return this.http.post(url, obj).map((res: Response) => {
    //         return res.json();
    //     });
    // }

    changeOwner(projectId, userId) {
        const obj = {
            user: userId
        };
        return this.http.post(environment.url + `/api/v1/projects/${projectId}/transfer_start`, { user: userId });
    }

    getProjectSize(planId) {
        return this.http.get(environment.url + '/api/v1/project-plans/' + planId).pipe(map((res: any) => {
            return res;
        }));
    }

    getProjectPlanList() {
        return this.http.get(environment.url + '/api/v1/project-plans').pipe(map((res: any) => {
            return res;
        }));
    }

    updateProject(projectId, data) {
        return this.http.put(environment.url + '/api/v1/projects/' + projectId, data).pipe(map((res: any) => {
            return res;
        }));
    }

    removeProject(projectId) {
        return this.http.delete(environment.url + '/api/v1/projects/' + projectId).pipe(map((res: any) => {
            return res;
        }));
    }
}
