export class ProjectSizeModal {
    duration: number;
    name: string;
    storage_size = 0;
    user_limit: number;
}
