import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SettingsRoutingModule } from './settings-routing.module';
import { ProjectComponent } from './project/project.component';
import { StatusComponent } from './status/status.component';
import { MembersComponent } from './members/members.component';
import { PermissionsComponent } from './permissions/permissions.component';
import { CoreModule } from '../core/core.module';
import { PaymentPlanComponent } from './payment-plan/payment-plan.component';

@NgModule({
  imports: [
    CommonModule,
    SettingsRoutingModule,
    CoreModule
  ],
  declarations: [ProjectComponent, StatusComponent, MembersComponent, PermissionsComponent,PaymentPlanComponent]
})
export class SettingsModule { }
