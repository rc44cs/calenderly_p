import { Component, OnInit } from '@angular/core';
import { Store } from '../../../../node_modules/@ngrx/store';
import { AppState } from '../../store/app.reducers';

@Component({
  selector: 'app-members',
  templateUrl: './members.component.html',
  styleUrls: ['./members.component.css']
})
export class MembersComponent implements OnInit {

  constructor(private store: Store<AppState>) { }
  project;
  ngOnInit() {
    this.store.select('main').subscribe(state => {
      this.project = state.project;
    })
  }
}
