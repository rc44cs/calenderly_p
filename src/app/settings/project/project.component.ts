import { config } from './../../providers/config';
import { AppState } from './../../store/app.reducers';
import { Store } from '@ngrx/store';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-project',
  templateUrl: './project.component.html',
  styleUrls: ['./project.component.css']
})
export class ProjectComponent implements OnInit {

  constructor(private store: Store<AppState>) { }
  project: any;
  images = config.images;
  ngOnInit() {
    this.store.select('main').subscribe(state => {
      this.project = state.project;
    })
  }

}
