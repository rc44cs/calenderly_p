import { ProjectComponent } from './project/project.component';
import { StatusComponent } from './status/status.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MembersComponent } from './members/members.component';
import { PermissionsComponent } from './permissions/permissions.component';
import { PaymentPlanComponent } from './payment-plan/payment-plan.component';

const routes: Routes = [
  {
    path: 'project',
    component: ProjectComponent
  },
  {
    path: 'status',
    component: StatusComponent
  },
  {
    path: 'members',
    component: MembersComponent
  },
  {
    path: 'payment-plan',
    component: PaymentPlanComponent
  },
  {
    path: 'permissions',
    component: PermissionsComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SettingsRoutingModule { }
