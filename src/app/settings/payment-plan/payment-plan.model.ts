export class ProjectPlanModel {
    duration: number;
    name: string;
    storage_size: number;
    user_limit: number;
}