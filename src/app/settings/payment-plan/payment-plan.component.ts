import { AppState } from './../../store/app.reducers';
import { Component, OnInit } from '@angular/core';
import { Store } from '../../../../node_modules/@ngrx/store';
import { SettingService } from '../shared/setting.service';
import { ProjectPlanModel } from './payment-plan.model';

@Component({
    selector: 'app-payment-plan',
    templateUrl: './payment-plan.component.html',
    styleUrls: ['./payment-plan.component.css']
})
export class PaymentPlanComponent implements OnInit {
    projectPlanList: Array<ProjectPlanModel> = [];
    projectPlan: any;
    constructor(
        private store: Store<AppState>,
        private settingService: SettingService
    ) { }
    template;
    project;
    ngOnInit() {
        this.store.select('main').subscribe(state => {
            this.project = state.project;
        });
        this.getProjectSize();
       
    }

    getProjectSize() {
        if (this.project.current_plan) {
            this.settingService.getProjectSize(this.project.current_plan).subscribe(res => {
                this.projectPlan = res;
                this.projectPlan.storage_size = +(this.projectPlan.storage_size / 1e+9).toFixed(1);
                this.getProjectPlan();
            });
        }
    }

    getProjectPlan() {
        this.settingService.getProjectPlanList().subscribe(plans => {
            this.projectPlanList = plans;
            this.projectPlanList.filter(list => {
                list.storage_size = +(list.storage_size / 1e+9).toFixed(1);
            });
        });
    }
}
