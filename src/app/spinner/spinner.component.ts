import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'spinner',
  templateUrl: './spinner.component.html',
  styleUrls: ['./spinner.component.scss']
})
export class SpinnerComponent implements OnInit {

  constructor() { }
  @Input() showSpinner;
  ngOnInit() {
  }
  show;
  ngOnChanges(change) {
    if (!change.isFirst) {
      this.show = change.showSpinner.currentValue
      // console.log(change);
    }
  }


}
