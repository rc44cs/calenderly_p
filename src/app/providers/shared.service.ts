import * as MainActions from './../layout/store/main.actions';
import { environment } from './../../environments/environment';
import { Router } from '@angular/router';
import { isArray } from 'util';
import { AppState } from './../store/app.reducers';
import { Subject, forkJoin } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { EventsService } from './events.service';
import { Store, State } from '@ngrx/store';
import { Ng2IzitoastService } from 'ng2-izitoast';

@Injectable()
export class SharedService {
    initialPath;
    sidebarVisible: boolean
    sidebarVisibilitySubject: Subject<boolean> = new Subject<boolean>()
    createPostSubject: Subject<boolean> = new Subject<boolean>();
    createTopicSubject: Subject<boolean> = new Subject<boolean>();
    maTheme: string
    maThemeSubject: Subject<string> = new Subject<string>()
    project;
    initialUrl;
    constructor(private http: HttpClient,
        private store: Store<AppState>,
        private router: Router,
        private state: State<AppState>,
        private eventsService: EventsService,
        public iziToast: Ng2IzitoastService) {
        this.sidebarVisible = false
        this.maTheme = 'green'
    };

    toggleSidebarVisibilty() {
        this.sidebarVisible = !this.sidebarVisible
        this.sidebarVisibilitySubject.next(this.sidebarVisible)
    }

    setTheme(color) {
        this.maTheme = color;
        this.maThemeSubject.next(this.maTheme);
    }

    getProjectTemplate() {
        return this.http.get(environment.url + '/api/v1/project-templates/1');
    }

    getInvitation(token) {
        return this.http.get(environment.url + '/api/v1/invitations/' + token);
    }

    createProject() {
        return this.getProjectTemplate().pipe(
            switchMap((template) => {
                return this.http.post(environment.url + `/api/v1/projects`, { name: 'Beta Project', description: 'Beta Desc.', creation_template: 1, is_private: true, current_plan: environment.plan.id })
            })
        )
    }

    addMember(data) {
        return this.http.post(environment.url + '/api/v1/memberships', data);
    }


    assignProject(id, user) {
        return this.getProject(id);
    }

    mapSearchResponse(obj) {
        var response = [];
        Object.keys(obj).forEach(key => {
            if (isArray(obj[key])) {
                obj[key].forEach(o => {
                    o.type = key;
                    response.push(o);
                })
            }
        });
        return response;
    }

    searchtext(text) {
        return this.http.get(environment.url + `/api/v1/search?project=${this.state.value.main.project.id}\&text=` + text)
            .pipe(map((res: any) => {
                res.Posts = res.userstories;
                res.Topics = res.epics;
                delete res.epics;
                delete res.userstories;
                return this.mapSearchResponse(res);
            }))
    }

    searchEpics(text) {
        return this.http.get(environment.url + `/api/v1/search?project=${this.state.value.main.project.id}\&text=` + text)
            .pipe(map((res: any) => res.epics))

    }

    getPostStatuses() {
        return this.http.get(environment.url + `/api/v1/userstory-statuses?project=` + this.state.value.main.project.id)
    }

    getTopicStatuses() {
        return this.http.get(environment.url + `/api/v1/epics?project=` + this.state.value.main.project.id)
    }


    notify(message, type?: string) {
        this.iziToast[type ? type : 'show']({ title: message, position: 'bottomLeft', id: 't_toast_notification' });

    }

    getUserDetails(id) {
        return this.http.get(environment.url + `/api/v1/users/` + id)
            .pipe(map((res: any) => {
                return res;
            }))
    }

    getMe() {
        return this.http.get(environment.url + '/api/v1/users/me').pipe(map((res: any) => {
            return res;
        }))
    }

    getProjects() {
        var user = this.state.value.main.user;
        return this.http.get(environment.url + '/api/v1/projects?member=' + user.id)
            .pipe(switchMap(res => this.http.get(environment.url + '/api/v1/projects/' + res[0].id)))
    }

    updateRegisteredUser(data) {
        data.existing = true;
        return this.http.post(environment.url + '/api/v1/auth/register', data).pipe(map((res: any) => {
            var state = this.state.value.main;
            window[state.storageStrategy].setItem('authToken', res.auth_token);
            this.store.dispatch(new MainActions.UserUpdated(res));
            this.store.dispatch(new MainActions.ChangeUiState({
                showTour: true
            }));
            return res;
        }));
    }


    updateUser(user) {
        return this.http.get(environment.url + '/api/v1/users/' + user.id);
    }


    getProject(id) {
        return this.http.get(environment.url + '/api/v1/projects/' + id);
    }

    ngOnInit() {
        this.project = this.state.value.main.project;
    }

}