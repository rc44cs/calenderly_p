import { AppState } from './../../store/app.reducers';
import { Router } from '@angular/router';
import { EventsService } from './../events.service';

import { Observable, empty } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpEvent, HttpClient, HttpErrorResponse, HttpInterceptor, HttpHandler, HttpRequest, HttpResponse } from '@angular/common/http';

import { State } from '@ngrx/store';
import { tap } from 'rxjs/operators';


@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  constructor(private router: Router,
    private state: State<AppState>) { };
  intercept(req: HttpRequest<any>, next: HttpHandler) {
    var state = this.state.value.main;
    let authReq;
    var token

    if (state.authHeader === 'Application') {
      token = window[state.storageStrategy].getItem('cypheredToken');
    }
    else if (state.authHeader === 'Bearer') {
      token = window[state.storageStrategy].getItem('authToken');
    }
    if (token) {
      authReq = req.clone({ headers: req.headers.set('Authorization', `${state.authHeader} ${token}`).set('Accept-Language', 'English (US)').set('x-disable-pagination','true') })
    }
    else {
      authReq = req.clone()
    }

    return next.handle(authReq)
      .pipe(tap((event: HttpEvent<any>) => {
        if (event instanceof HttpResponse) {
        }
      }, (err: any) => {
        if (err instanceof HttpErrorResponse) {
          if (err.status === 401) {
            this.router.navigate(['/', 'pages', 'login']);
            window[state.storageStrategy].removeItem('authToken');
            window[state.storageStrategy].removeItem('auth_code');
          }
        }
        return empty();
      }));

  }
}


