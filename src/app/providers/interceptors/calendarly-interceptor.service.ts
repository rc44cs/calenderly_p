import { tap } from 'rxjs/operators';
import { PostService } from './../../post/post.service';
import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { HttpEvent, HttpClient, HttpErrorResponse, HttpInterceptor, HttpHandler, HttpRequest, HttpResponse } from '@angular/common/http';

@Injectable()
export class CalendarlyInterceptor implements HttpInterceptor {
    constructor(private router: Router, private postService: PostService) { };
    
    intercept(req: HttpRequest<any>, next: HttpHandler) {
        var body = req.body;
        var authReq;
        var isCalendarlyItem = (req.method === 'POST' || req.method === 'PATCH' || req.method === 'PUT')
            && req.headers.get('Authorization') && body && body.title
        if (isCalendarlyItem) {
            authReq = req.clone({
                body: this.postService.mapPostToCalendarly(body)
            });
        } 
        else {
            authReq = req.clone();
        }
        authReq.headers.append('Accept-Language', 'English (US)');
        authReq.headers.append('x-disable-pagination','true')
        return next.handle(authReq).pipe(tap(e => {
            if (e instanceof HttpResponse) {
                if (isCalendarlyItem) {
                    console.log('sachi!')
                    var response = e.clone({
                        body: this.postService.mapPostResponse(e.clone().body)
                    });
                }
                return response;
            }
        }))
    }
}