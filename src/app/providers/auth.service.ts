import { environment } from './../../environments/environment';
import * as MainActions from './../layout/store/main.actions';
import { AppState } from './../store/app.reducers';
import { SharedService } from './shared.service';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, switchMap, map, mergeMap } from 'rxjs/operators';
import { Store, State } from '@ngrx/store';
import { getScripts } from './get-scripts';
import { of } from '../../../node_modules/rxjs';
declare var $: any;

@Injectable()
export class AuthService {

  constructor(private http: HttpClient,
    private router: Router, private sharedService: SharedService,
    private store: Store<AppState>, private state: State<AppState>,
    private route: ActivatedRoute) { }

  authenticateUser(data) {
    var state = this.state.value.main;
    state.authHeader = 'Bearer'
    return this.http.post(environment.url + '/api/v1/auth', data)
      .pipe(switchMap((res: any) => {
        window[state.storageStrategy].setItem('authToken', res.auth_token);
        this.store.dispatch(new MainActions.UserUpdated(res));
        this.store.dispatch(new MainActions.AuthenticateUser({ isAuthenticated: true }))
        return this.getToken();
      }))
  }

  forgotPassword(userInfo) {
    return this.http.post(environment.url + '/api/v1/users/password_recovery', { username: userInfo })
      .pipe(map(res => {
        return res;
      }))
  }


  changePassword(data) {
    return this.http.post(environment.url + '/api/v1/users/change_password_from_recovery', data)
      .pipe(map(res => {
        return res;
      }))
  }

  signupUser(data) {
    var state = this.state.value.main;
    return this.http.post(environment.url + '/api/v1/auth/register', data)
      .pipe(map((res: any) => {
        window[state.storageStrategy].setItem('authToken', res.auth_token);
        this.store.dispatch(new MainActions.UserUpdated(res));
        this.store.dispatch(new MainActions.ChangeUiState({
          showTour: true
        }));
        return res;
      }), mergeMap(res => {
        if (!state.invitation.id) {
          return this.sharedService.createProject();
        }
        else {
          return this.sharedService.assignProject(state.invitation.project, res.id);
        }
      }));
  }



  getToken() {
    var state = this.state.value.main;
    var data = {
      "application": state.appId,
      "state": state.appState
    }
    return this.http.post(environment.url + `/api/v1/application-tokens/authorize`, data)
      .pipe(switchMap((res: any) => {
        window[state.storageStrategy].setItem('auth_code', res.auth_code);
        this.store.dispatch(new MainActions.SetAuthParameters({ auth_code: res.auth_code, isAuthenticated: true }));
        return this.validateToken();
      }))
  }

  getUserDetails() {
    return this.http.get(environment.url + `/api/v1/users/me`)
      .pipe(map((res: any) => {
        this.store.dispatch(new MainActions.UserUpdated(res));
        return res;
      }))
  }

  validateToken() {
    var state = this.state.value.main;

    var authCode = window[state.storageStrategy].getItem('auth_code');

    var data = {
      "application": state.appId,
      "auth_code": authCode,
      "state": state.appState
    }

    return this.http.post(environment.url + `/api/v1/application-tokens/validate`, data)
      .pipe(map((res: any) => {
        window[state.storageStrategy].setItem('cypheredToken', res.token);
        this.store.dispatch(new MainActions.SetAuthParameters({
          authHeader: 'Application',
          cypheredToken: res.token
        }));
        return res;
      }), switchMap(res => this.sharedService.getMe()), map(res => {
        console.log(window.location.pathname, 'path');
        $('.scripttemp').remove();
        $('head').append(getScripts(res.id));
        environment.scripts.forEach(s => {
          $('head').append(s);
        })
        this.store.dispatch(new MainActions.UserUpdated(res))
        return res;
      }))

  }

  handleError(er) {
    var state = this.state.value.main;
    if (er.status === 401) {
      this.router.navigate(['/', 'pages', 'login']);
      window[state.storageStrategy].removeItem('authToken');
      window[state.storageStrategy].removeItem('auth_code');
    }
  }

}
