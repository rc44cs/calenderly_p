import { environment } from "../../environments/environment";

export const config = {
    images:{
        fallbackImg:'./assets/img/profile-pics/2.jpg',
        fileFallback:'./assets/img/file.png',
        profileImage:null,
        navLogo:'./assets/img/nav-logo.png'
    }
}