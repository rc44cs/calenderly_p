import * as MainActions from './../layout/store/main.actions';
import { AppState } from './../store/app.reducers';
import { AuthService } from './auth.service';
import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { State, Store } from '@ngrx/store';
import { SharedService } from './shared.service';

@Injectable({
  providedIn: 'root'
})
export class TokenGuard implements CanActivate {
  constructor(private as: AuthService, private state: State<AppState>,
    private store: Store<AppState>, private router: Router,
    private ss: SharedService) { }
  async canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot) {
    var params = next.params || next.queryParams;
    console.log(state, 'state');
    var token = params.token;
    var validate;
    var isValid = false;
    try {
      if (state.url.includes('invitation')) {
        validate = await this.ss.getInvitation(token).toPromise();
        if (validate.id) {
          this.store.dispatch(new MainActions.UpdateInvitation(validate));
          isValid = true;
        }
      }
      if (isValid) {
        return isValid;
      }
      else {
        this.router.navigate(['/', 'pages', 'login']);
      }
    }
    catch (er) {
      this.router.navigate(['/', 'pages', 'login']);
    }

  }

}
