import { environment } from './../../environments/environment';
export function getScripts(userId) {
    var planId = environment.plan.id;
    return `
    <script class="scripttemp">
    window.dataLayer = window.dataLayer || [];
    window.dataLayer.push({
        Plan: ${planId},
        userID: ${userId}
    });
</script>
`
}