import { EventsService } from './events.service';
import { AppState } from './../store/app.reducers';
import { State } from '@ngrx/store';
import { AuthService } from './auth.service';
import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { SharedService } from './shared.service';

@Injectable()
export class AuthGuard implements CanActivate {

  constructor(private authService: AuthService, private shared: SharedService,
    private router: Router, private state: State<AppState>, private eventsService: EventsService) { }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    this.shared.initialPath = state.url;
    return Observable.create(observer => {
      this.authService.getToken().subscribe(res => {
        observer.next(true);
        observer.complete();
      }, er => {
        this.eventsService.exitUser.next()
        observer.next(false);
        observer.complete();
      })
    })
  }
}
