import { environment } from './../../environments/environment';
import { Injectable } from '@angular/core';
import { SwUpdate } from '@angular/service-worker';
declare var swal: any;
@Injectable()
export class ServiceWorkerService {

  constructor(private updates: SwUpdate) { }

  ngOnInit() {
    if (environment.production && this.updates.isEnabled) {
      this.updates.available.subscribe(() => {
        swal({
          title: 'New app version available!',
          text: "Load new version?",
          type: 'primary',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Yes, download it!'
        }).then((result) => {
          if (result.value) {
            window.location.reload();
          }
        })
      })
    }
    this.updates.available.subscribe((e: any) => {
      console.log('current version is', e.current);
      console.log('available version is', e.available);
    })
    this.updates.activated.subscribe(event => {
      console.log('old version was', event.previous);
      console.log('new version is', event.current);
    });
  }


}
