import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
declare var $: any;
import { BehaviorSubject } from 'rxjs';


@Injectable()
export class EventsService {

  toggleChat = new Subject<any>()
  toggleSidebar = new Subject<any>();
  hideTopic = new Subject<any>();
  openTopic = new Subject<any>();
  openPostSubject = new Subject<any>();
  hidePostSubject = new Subject<any>();
  openMenuSubject = new Subject<any>();
  openAddMember = new Subject<any>();
  hideAddMember = new Subject<any>();
  exitUser = new Subject<any>();
  printScript = new Subject<any>();
  clickAddBtn = new Subject<any>();
  data: any;
  private postDataSource = new BehaviorSubject(this.data);
  currentData = this.postDataSource.asObservable();

  constructor() { }

  changeData(data) {
    this.postDataSource.next(data);
  }
}
