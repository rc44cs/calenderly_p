import { EventsService } from './../providers/events.service';
import { config } from './../providers/config';
import { AppState } from './../store/app.reducers';
import { Store } from '@ngrx/store';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'profile1',
  templateUrl: './profile.component.html'
})

export class ProfileComponent implements OnInit {
  images=config.images;
  constructor(private eventsService:EventsService,
  private store:Store<AppState>) { }
  data=[];
  user;

  ngOnInit() {
    // this.user=this.container.user;
    this.userUpdated();
  }
  userUpdated()
  {
    this.store.select('main').subscribe(state=>{
      this.user=state.user;
    })
  }

}
