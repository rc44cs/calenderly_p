import { ChangePasswordComponent } from './change-password/change-password.component';
import { CoreModule } from './../core/core.module';
import { SignupComponent } from './signup/signup.component';
import { LoginComponent } from './login/login.component';
import { PagesRoutingModule } from './pages.routing.module';
import { ModalModule } from 'ngx-bootstrap/modal';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { PageHeaderComponent } from '../page-header/page-header.component';
import { PageFooterComponent } from '../page-footer/page-footer.component';
import { InvitationComponent } from './invitation/invitation.component';


@NgModule({
    declarations: [
        LoginComponent,
        SignupComponent,
        PageHeaderComponent,
        PageFooterComponent,
        ChangePasswordComponent,
        InvitationComponent
    ],
    imports: [
        CommonModule,
        CoreModule,
        PagesRoutingModule
    ]
})

export class PagesModule { }