import * as MainActions from './../../layout/store/main.actions';
import { SharedService } from './../../providers/shared.service';
import { AuthService } from './../../providers/auth.service';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { config } from './../../providers/config';
import { AppState } from './../../store/app.reducers';
import { Component, OnInit } from '@angular/core';
import { State, Store } from '../../../../node_modules/@ngrx/store';
import { Router, ActivatedRoute } from '../../../../node_modules/@angular/router';
import { validatePassword } from '../password-validator';

@Component({
  selector: 'app-invitation',
  templateUrl: './invitation.component.html',
  styleUrls: ['./invitation.component.scss']
})
export class InvitationComponent implements OnInit {

  uiState;
  signupForm: FormGroup;
  constructor(private fb: FormBuilder, private authService: AuthService,
    private router: Router, private state: State<AppState>, private route: ActivatedRoute,
    private sharedService: SharedService, private store: Store<AppState>) { }

  invitation
  images = config.images;

  ngOnInit() {
    this.store.select('main').subscribe(state => {
      this.invitation = state.invitation;
      this.uiState = state.uiState;
    })
    this.initForm();
  }

  initForm() {
    this.signupForm = this.fb.group({
      username: this.fb.control(null, [Validators.required]),
      full_name: this.fb.control(null, [Validators.required]),
      email: this.fb.control(null, [Validators.required, Validators.email]),
      password: this.fb.group({
        password: this.fb.control(null, [Validators.required, Validators.minLength(5)]),
        confirmPassword: this.fb.control(null, [Validators.required, Validators.minLength(5)])
      }, {
          validator: validatePassword
        }),
      subscription: this.fb.control(false)
    })
  }

  submitForm() {
    var state = this.state.value.main;
    this.store.dispatch(new MainActions.ChangeUiState({ signup: true }));
    var userData = { ...this.signupForm.value };
    userData.password = userData.password.password;
    console.log(userData, 'userdata')
    if (this.signupForm.valid) {
      this.sharedService.updateRegisteredUser({ token: this.route.snapshot.params.token, type: 'private', ...userData }).subscribe((res: any) => {
        this.router.navigate(['/', 'calendar']);
        this.store.dispatch(new MainActions.ChangeUiState({ signup: false }));
      })
    }
    else {
      this.sharedService.notify('Please enter valid details')
    }

  }


}
