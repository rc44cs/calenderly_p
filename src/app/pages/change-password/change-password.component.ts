import { ActivatedRoute } from '@angular/router';
import * as MainActions from './../../layout/store/main.actions';
import { AppState } from './../../store/app.reducers';
import { Store } from 'node_modules/@ngrx/store';
import { DragTopicSuccess } from './../../post/store/post.actions';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { Component, OnInit, ViewChild } from '@angular/core';
import { validatePassword } from '../password-validator';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss']
})
export class ChangePasswordComponent implements OnInit {

  constructor(private fb: FormBuilder, private store: Store<AppState>, private route: ActivatedRoute) { }
  @ViewChild('forgotPasswordModal') forgotPasswordModal: ModalDirective;
  passwordForm: FormGroup;
  ngOnInit() {
    this.passwordForm = this.fb.group({
      password: this.fb.control(null, [Validators.required, Validators.minLength(6)]),
      confirmPassword: this.fb.control(null, [Validators.required, Validators.minLength(6)])
    }, {
        validator: validatePassword
      })
  }

  changePassword() {
    if (this.passwordForm.valid) {
      var val = this.passwordForm.value;
      console.log(this.route.snapshot, 'params');
      console.log(this.passwordForm.value);
      this.store.dispatch(new MainActions.ChangePassword({
        token: this.route.snapshot.params.token,
        password: val.password
      }))
    }

  }

}
