import { InvitationComponent } from './invitation/invitation.component';
import { NgModule } from '@angular/core';
import { LoginComponent } from './login/login.component';
import { SignupComponent } from './signup/signup.component';
import { Routes, RouterModule } from '@angular/router';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { TokenGuard } from '../providers/token.guard';


const PagesRoutes: Routes = [
    {
        path: '',
        children: [
            {
                path: 'login',
                component: LoginComponent
            },
            {
                path: 'signup',
                component: SignupComponent
            },
            {
                path: 'change-password/:token',
                component: ChangePasswordComponent,
                // canActivate: [TokenGuard]
            },
            {
                path: 'invitation/:token',
                component: InvitationComponent,
                canActivate: [TokenGuard]
            }
        ]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(PagesRoutes)
    ],
    exports: [
        RouterModule
    ]
})
export class PagesRoutingModule { }

