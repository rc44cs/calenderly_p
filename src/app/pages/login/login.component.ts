import * as MainActions from './../../layout/store/main.actions';
import { AppState } from './../../store/app.reducers';
import { SharedService } from './../../providers/shared.service';

import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Component, OnInit, ViewChild } from '@angular/core';

import { ModalDirective } from 'ngx-bootstrap'
import { Store, State } from '@ngrx/store';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  constructor(private fb: FormBuilder, private router: Router, private sharedService: SharedService,
    private state: State<AppState>,
    private store: Store<AppState>) { }

  loginForm: FormGroup;
  @ViewChild('forgotPasswordModal') forgotPasswordModal: ModalDirective;
  rememberMe = false;
  username;
  uiState;

  initForm() {
    this.loginForm = this.fb.group({
      username: this.fb.control(null, [Validators.required]),
      password: this.fb.control(null, [Validators.required])
    })
  }

  emailPassword(username) {
    this.store.dispatch(new MainActions.ChangeUiState({ login: true }))
    if (username) {
      this.store.dispatch(new MainActions.ForgotPassword(username));
      this.forgotPasswordModal.hide();
    }
  }

  submitForm() {
    this.store.dispatch(new MainActions.ChangeUiState({ login: true }))
    var state = this.state.value.main;
    var userData = this.loginForm.value;
    userData.type = 'normal'
    if (this.rememberMe) {
      state.storageStrategy = 'localStorage';
      window[state.storageStrategy].setItem('storageStrategy', state.storageStrategy);
    }
    this.store.dispatch(new MainActions.LoginUser(userData))
  }

  createAccount() {
    this.router.navigate(['/', 'pages', 'signup'])
  }

  ngOnInit() {
    console.log('in login')
    this.initForm();
    this.store.select('main').subscribe(state => {
      this.uiState = state.uiState;
    })
  }
}
