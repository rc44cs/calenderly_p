import * as MainActions from './../../layout/store/main.actions';
import { AppState } from './../../store/app.reducers';
import { Router } from '@angular/router';
import { AuthService } from '../../providers/auth.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { SharedService } from '../../providers/shared.service';
import { Store } from '@ngrx/store';
import { validatePassword } from '../password-validator';


@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {
  policy = false;
  emailPattern = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    private router: Router,
    private sharedService: SharedService,
    private store: Store<AppState>
  ) { }
  signupForm: FormGroup;

  login() {
    this.router.navigate(['/', 'pages', 'login'])
  }



  initForm() {
    this.signupForm = this.fb.group({
      username: this.fb.control(null, [Validators.required]),
      full_name: this.fb.control(null, [Validators.required]),
      type: this.fb.control('public', [Validators.required]),
      email: this.fb.control(null, [Validators.required, Validators.email]),
      password: this.fb.group({
        password: this.fb.control(null, [Validators.required, Validators.minLength(5)]),
        confirmPassword: this.fb.control(null, [Validators.required, Validators.minLength(5)])
      }, {
          validator: validatePassword
        }),
      subscription: this.fb.control(false),

    })
  }

  showMessage(message, type) {
    this.sharedService.notify(message, type);

  }

  submitForm() {
    if (this.signupForm.value.username && this.signupForm.value.username.match(' ')) {
      this.showMessage('Space not allowed in username', 'info');
      return;
    }
    if (this.signupForm.value.email && !this.emailPattern.test(this.signupForm.value.email)) {
      this.showMessage('Email field needs to be a valid email', 'info');
      return;
    }
    if (this.signupForm.value.password.password && this.signupForm.value.password.password.length < 6) {
      this.showMessage('Password must be at least 6 characters long', 'info');
      return;
    }
    if (this.signupForm.value.password.password !== this.signupForm.value.password.confirmPassword) {
      this.showMessage('Please, make sure the passwords match', 'info');
      return;
    }

    if (!this.signupForm.valid) {
      this.showMessage("All fields are mandatory, please, don't leave any empty fields", 'info');
      return;
    }
    if (!this.policy) {
      this.showMessage("Please accept the Privacy Policy", 'info');
      return;
    }

    this.store.dispatch(new MainActions.ChangeUiState({ signup: true }));
    const userData = { ...this.signupForm.value };
    userData.password = userData.password.password;
    if (this.signupForm.valid) {
      this.store.dispatch(new MainActions.SignupUser(userData));
    }
  }
  uiState;


  ngOnInit() {
    this.initForm();
    this.store.select('main').subscribe(state => {
      this.uiState = state.uiState;
    })
  }

}
