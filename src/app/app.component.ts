import { environment } from './../environments/environment';
import * as MainActions from './layout/store/main.actions';
import { AppState } from './store/app.reducers';
import { Store, State } from '@ngrx/store';
import { SharedService } from './providers/shared.service';
import { Router, NavigationStart, NavigationEnd } from '@angular/router';
import { Component, enableProdMode } from '@angular/core';
import { EventsService } from './providers/events.service';
declare var $: any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent {
  constructor(private eventsService: EventsService, private sharedService: SharedService,
    private router: Router,
    private store: Store<AppState>,
    private state: State<AppState>) { }

  ngOnInit() {
    // this.sharedService.initialPath = location.pathname !== '/pages/login' ? location.pathname : null;

    this.exitUser();
    this.router.events.subscribe(e => {
      if (e instanceof NavigationStart) {
      }
      if (e instanceof NavigationEnd) {
        this.sharedService.sidebarVisibilitySubject.next(false);
      }
    })

    window.addEventListener('online', (e) => {
      this.sharedService.setTheme('green');
      this.sharedService.notify('You are now Online')
      this.store.dispatch(new MainActions.IsOnline(true));
    })

    window.addEventListener('offline', (e) => {
      this.sharedService.setTheme('red');
      this.sharedService.notify('You are Offline')
      this.store.dispatch(new MainActions.IsOnline(false));
    })

  }
  exitUser() {
    var state = this.state.value.main;
    this.eventsService.exitUser.subscribe(() => {
      console.log('exiting');
      $('.scripttemp').remove();
      // $('#OzCNytY-1542091457585').css('left', '200rem !important');
      window[state.storageStrategy].removeItem('cypheredToken');
      window[state.storageStrategy].removeItem('authToken');
      window[state.storageStrategy].removeItem('auth_code');
      this.store.dispatch(new MainActions.LogoutUser());
      this.sharedService.notify('Please login to continue');
      this.router.navigate(['/', 'pages', 'login']);
    });
  }

  ngAfterViewInit(): void {
    //Called after ngAfterContentInit when the component's view has been initialized. Applies to components only.
    //Add 'implements AfterViewInit' to the class.
    // $('#OzCNytY-1542091457585').css('left', '200rem');
  }

}

