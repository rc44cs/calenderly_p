import { SharedService } from './../../providers/shared.service';
import * as MainActions from './../../layout/store/main.actions';
import * as PostActions from './../../post/store/post.actions';
import { PostService } from './../../post/post.service';
import { AppState } from './../../store/app.reducers';
import { Store } from '@ngrx/store';
import { DragulaService } from 'ng2-dragula';
import { config } from './../../providers/config';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { EventsService } from '../../providers/events.service';
import { AutoUnsubscribe } from '../../layout/auto-unsub';
declare var swal: any;
@Component({
  selector: 'kanban-columns',
  templateUrl: './kanban-columns.component.html',
  styleUrls: ['./kanban-columns.component.scss']
})
export class KanbanColumnsComponent implements OnInit {

  constructor(private router: Router, private eventsService: EventsService,
    private dragulaService: DragulaService, private store: Store<AppState>,
    private postService: PostService,
    private shared: SharedService) { }

  statuses = [];
  fallbackImg = config.images.fallbackImg;
  subscriptions = [];
  loading;
  ngOnInit() {

    this.store.select('main').subscribe(state => {
      this.loading = state.uiState.kanban;
    })

    this.store.select('post').select('statuses').subscribe(statuses => {
      this.statuses = statuses;
      // console.log(statuses,)
    })
    this.subscriptions[0] = this.dragulaService.drop.subscribe(res => {
      var post = JSON.parse(res[1].id);
      var order = Number(res[1].parentNode.previousSibling.firstChild.id)
      if (post.status !== order) {
        this.store.dispatch(new PostActions.DragPost({
          id: post.id,
          prevStatus: post.status,
          data: {
            id: post.id,
            status: order,
            version: Number(post.version)
          }
        }));
      }
    })
  }

  deletePost(id) {
    swal({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      reverseButtons: true,
      confirmButtonText: 'Yes, delete it!'
    })
      .then(result => {
        if (result.value) {
          this.store.dispatch(new MainActions.ChangeUiState({
            kanban: true
          }))
          this.store.dispatch(new PostActions.DeletePost(id));
        }
      })
  }

  addPost(id) {
    console.log(id, 'p')
    this.eventsService.openPostSubject.next({ status: Number(id) });
  }

  editPost(ref) {
    this.store.dispatch(new MainActions.ChangeUiState({
      kanban: true
    }))
    this.router.navigate(['/', 'post', ref])
  }

  ngOnDestroy() {
    this.subscriptions.forEach((s: any) => {
      s.unsubscribe()
    })
  }

}
