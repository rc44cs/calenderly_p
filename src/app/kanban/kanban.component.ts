import * as MainActions from './../layout/store/main.actions';
import * as PostActions from './../post/store/post.actions';
import { PostService } from './../post/post.service';
import { Store } from '@ngrx/store';
import { AppState } from './../store/app.reducers';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { Component, OnInit, ViewChild } from '@angular/core';
import { DragulaService } from 'ng2-dragula';
import { AutoUnsubscribe } from '../layout/auto-unsub';

@Component({
  selector: 'app-kanban',
  templateUrl: './kanban.component.html',
  styleUrls: ['./kanban.component.scss']
})
@AutoUnsubscribe
export class KanbanComponent {
  constructor(private store: Store<AppState>, private postService: PostService) { }
  statuses = [];
  subscriptions = [];

  ngOnInit() {
    this.subscriptions[0] = this.store.select('post').subscribe(state => {
      this.statuses = state.statuses;
    })
  }

  ngOnDestroy() { }

}