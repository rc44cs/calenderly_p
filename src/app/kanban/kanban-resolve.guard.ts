import * as PostActions from './../post/store/post.actions';
import { Store, State } from '@ngrx/store';
import { SharedService } from './../providers/shared.service';
import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Resolve } from '@angular/router';
import { Observable ,  forkJoin } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { AppState } from '../store/app.reducers';
@Injectable()
export class KanbanResolveGuard implements Resolve<any> {
  constructor(private sharedService:SharedService,
private store:Store<AppState>,private state:State<AppState>){};
project;
 
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> {
      this.project=this.state.value.main.project;
    console.log('hello111')
    var observables=[];
    observables[0]=this.sharedService.getPostStatuses()
    .pipe(map((res:any[])=>res.filter(item=>item.project===this.state.value.main.project.id)))

    return forkJoin(observables).pipe(map((results:any[])=>{
        var statuses=results[0];
            var posts=this.state.value.post.posts.slice();
            statuses.forEach(o=>{
                o.data=[];
                posts.forEach(p=>{
                    if(p.status===o.id)
                    {
                        o.data.push(p);
                    }
                })
            })
            this.store.dispatch(new PostActions.SetStatuses(statuses));
            return {data:statuses};
    }))

  }
}
