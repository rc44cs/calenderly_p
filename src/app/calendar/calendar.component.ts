import { environment } from './../../environments/environment';
import { AppState } from './../store/app.reducers';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import * as moment from 'moment'
import { AutoUnsubscribe } from '../layout/auto-unsub';
import { TourService } from '../../../node_modules/ngx-tour-core';

@Component({
  selector: 'app-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.scss']
})
@AutoUnsubscribe
export class CalendarComponent {
  constructor(private store: Store<AppState>, public ts: TourService) { }
  subscriptions = [];
  uiState: any = {};
  date = new Date();

  ngOnInit() {
    this.subscriptions[0] = this.store.select('post').subscribe(state => {
      this.uiState = state.uiState;
    })
  }

  ngOnDestroy() { }



}
