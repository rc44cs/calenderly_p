import { map } from 'rxjs/operators';
import { PostService } from './../post/post.service';
import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Resolve } from '@angular/router';
import { Observable ,  forkJoin } from 'rxjs';
@Injectable()
export class CalendarResolveGuard implements Resolve<any> {
  constructor(private postService:PostService){}
 
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> {
    console.log('hello')
    var observables=[];
    observables[0]=this.postService.getPosts()

    return forkJoin([...observables])
    .pipe(map(results=>{
      return {
        posts:results[0]
      }
    }));
  }
}
