import { SharedService } from './../providers/shared.service';
import { HttpClient } from '@angular/common/http';
import { PostService } from './../post/post.service';
import { Injectable } from '@angular/core';
// import 'rxjs/operators';
// import 'rxjs'
import { catchError } from 'rxjs/operators';
import { environment } from '../../environments/environment';
@Injectable()
export class CalendarService {

  constructor(private postService:PostService,private http:HttpClient,
  private sharedService:SharedService) { }

  addMember(data)
  {
    return this.http.post(environment.url+'/api/v1/memberships/bulk_create',data);
  }

}
