import { EventsService } from './../../providers/events.service';
import { AppState } from './../../store/app.reducers';
import { Store } from '@ngrx/store';
import { Component, OnInit, Input, Output, ChangeDetectionStrategy, EventEmitter, ViewChild, ViewChildren, ElementRef } from '@angular/core';
import * as moment from 'moment';
import { Router } from '@angular/router';
import { AutoUnsubscribe } from '../../layout/auto-unsub';
import { CalendarService } from '../calendar.service';
declare var $: any;

@Component({
  selector: 'fullcalendar',
  templateUrl: './fullcalendar.component.html',
  styleUrls: ['./fullcalendar.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
@AutoUnsubscribe
export class FullcalendarComponent implements OnInit {

  constructor(private router: Router, private store: Store<AppState>,
    private eventsService: EventsService,
    private calendarService: CalendarService) { }
  @Input() posts = [];
  @Input() date;
  subscriptions = [];
  fullCalendar;
  @ViewChild('calendar') calendar: ElementRef;
  ngOnInit() {
    this.fullCalendar = $(this.calendar.nativeElement)
    // this.initCalendar(this.posts);
    this.subscriptions[0] = this.store.select('post').select('posts').subscribe(posts => {
      this.initCalendar(posts);
    })
  }

  initCalendar(posts?: any[]) {
    this.fullCalendar.fullCalendar('destroy');
    var height;
    if (window.innerWidth > 600) {
      height = undefined;
    }
    else {
      height = 650;
    }

    this.fullCalendar.fullCalendar({
      header: {
        right: '',
        center: 'prev, title, next',
        left: ''
      },
      eventLimit: true,
      firstDay: 1,
      themeSystem: 'bootstrap4',
      height: height,
      theme: true, //Do not remove this as it ruin the design
      selectable: true,
      selectHelper: true,
      handleWindowResize: true,
      editable: true,
      defaultDate: moment(this.date),
      events: posts ? posts : [],
      eventClick: (calEvent, jsEvent, view) => {
        // console.log(calEvent, 'calevent')
        this.eventsService.changeData(1);
        this.router.navigate(['/', 'post', calEvent.ref]);

      },
      select: (start, end, allDay) => {
        this.eventsService.changeData(0);
        this.eventsService.openPostSubject.next({ date: start.toDate(), time: start.toDate() });

      },
      dayClick: (date, jsEvent, view) => {
        this.date = date.toDate();
        $(this).css('background-color', 'red');

      }
    });
  }
}
