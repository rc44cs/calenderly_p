import { SharedService } from './../../providers/shared.service';
import { Router } from '@angular/router';

import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
declare var $: any;
import * as moment from 'moment';
import * as _ from 'underscore';
import { CalendarService } from '../../calendar/calendar.service';
import { State, Store } from '@ngrx/store';
import { AppState } from '../../store/app.reducers';
declare var swal: any;

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.scss']
})
export class AddUserComponent {
  project;
  constructor(private fb: FormBuilder,
    private calendarService: CalendarService, private state: State<AppState>,
    private store: Store<AppState>, private shared: SharedService) { }
  roles = [];
  @Output() close: EventEmitter<any> = new EventEmitter();
  role;
  username;
  ngOnInit() {
    this.project = this.state.value.main.project;;
    this.store.select('main').subscribe(state => {
      // console.log(state, 'roles');
      this.project = state.project;
      this.roles = this.project.roles;
    })
  }

  addMember() {
    console.log(this.username)
    this.calendarService.addMember({
      project_id: this.state.value.main.project.id,
      bulk_memberships: [{
        role_id: Number(this.role),
        username: this.username
      }]
    }).subscribe(res => {
      this.shared.notify('Invitation Sent!', 'success');

      this.close.emit();
    }, () => {
      this.shared.notify('Invitation not Sent!', 'error');

    })
  }


}
