import { TourService } from 'ngx-tour-ngx-bootstrap';
import * as PostActions from './../../post/store/post.actions';
import { EventsService } from './../../providers/events.service';
import { SharedService } from './../../providers/shared.service';
import { AppState } from './../../store/app.reducers';
import { Store, State } from '@ngrx/store';
import { Router, ActivatedRoute } from '@angular/router';
import { config } from './../../providers/config';
import { PostService } from './../../post/post.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { AutoUnsubscribe } from '../../layout/auto-unsub';

@Component({
    selector: 'app-create-topic',
    templateUrl: './create-topic.component.html',
    styleUrls: ['./create-topic.component.scss']
})
export class CreateTopicComponent {

    constructor(private fb: FormBuilder, private store: Store<AppState>,
        private router: Router, private sharedService: SharedService,
        public ts: TourService,
        private eventsService: EventsService, private route: ActivatedRoute,
        private postService: PostService, private state: State<AppState>
    ) { }
    editMode = false;
    createTopic: FormGroup;
    topicData;
    @Input() date;
    subscriptions = [];
    @Input() status;
    @Input() time;
    @Output() exit: EventEmitter<any> = new EventEmitter();
    color = { name: 'indigo' };
    tag;
    tags = ['Tag-A', 'Tag-B'];
    project;
    description;
    user;
    uiState;

    close() {
        if (this.editMode) {
            this.router.navigate(['/', 'calendar'])
        }
        else {
            this.eventsService.hideTopic.next();
        }
    }

    initForm() {
        this.createTopic = this.fb.group({
            title: this.fb.control(null, [Validators.required]),
            tags: this.fb.control(['Tag-A', 'Tag-B']),
            color: this.fb.control(null),
            description: this.fb.control(null)
        })
    }

    submitForm() {
        this.createTopic.patchValue({
            description: this.description,
            tags: this.tags
        })
        var topic = this.createTopic.value
        topic.subject = topic.title;
        topic.assigned_to = this.user.id;
        topic.project = this.state.value.main.project.id;
        topic.color = this.color.name;
        if (this.createTopic.valid) {
            this.store.dispatch(new PostActions.UiHttp({ calendar: true }));
            this.store.dispatch(new PostActions.CreateTopic(topic));
            this.eventsService.hideTopic.next();
            this.createTopic.reset();
            this.tags = ['Tag-A', 'Tag-B'];
            this.color = { name: 'indigo' };
            this.exit.emit();
            this.description = '';
            this.ts.next();
        }
        else {
            this.sharedService.notify('Please enter valid information')
        }
    }

    updateTopic() {
        var topic = {
            ...this.topicData,
            ...this.createTopic.value,
            tags: this.tags,
            color: this.color.name,
            description: this.description,
        }
        this.store.dispatch(new PostActions.UiHttp({
            topic: true
        }))
        this.store.dispatch(new PostActions.UpdateTopic(topic))
    }

    removeTag(index) {
        this.tags.splice(index, 1);
    }

    addTag(tag) {
        if (!this.tag) {
            return false;
        }
        if (this.tags.indexOf(tag) === -1) {
            this.tags.push(tag);
            this.tag = ''
        }
    }

    setColor(color) {
        this.color = color;
    }

    loadTopic(topic) {
        this.topicData = topic;
        this.tags = topic.tags;
        console.log(topic.tags)
        this.color.name = topic.color;
        this.createTopic.patchValue(this.topicData);
        this.description = topic.description;
    }

    quillConfig = {
        toolbar: [
            ['bold', 'italic', 'underline'],        // toggled buttons
            //   ['blockquote', 'code-block'],

            //   [{ 'header': 1 }, { 'header': 2 }],               // custom button values
            [{ 'list': 'ordered' }, { 'list': 'bullet' }],
            //   [{ 'script': 'sub'}, { 'script': 'super' }],      // superscript/subscript
            [{ 'indent': '-1' }, { 'indent': '+1' }],          // outdent/indent
            //   [{ 'direction': 'rtl' }],                         // text direction

            //   [{ 'size': ['small', false, 'large', 'huge'] }],  // custom dropdown
            //   [{ 'header': [1, 2, 3, 4, 5, 6, false] }],

            [{ 'color': [] }, { 'background': [] }],          // dropdown with defaults from theme
            // [{ 'font': [] }, 'link', 'image', 'video'],
            [{ 'align': [] }],

            ['clean'],                                         // remove formatting button

            //   ['link', 'image', 'video']                         // link and image, video
        ]
    };

    ngOnInit() {

        this.project = this.state.value.main.project;
        this.user = this.state.value.main.user;
        this.initForm();
        this.subscriptions[0] = this.store.select('post').subscribe(state => {
            this.topicData = state.currentTopic;
        })

        this.subscriptions[1] = this.store.select('main').subscribe(state => {
            this.user = state.user;
            this.uiState = state.uiState;
        })

        this.subscriptions[2] = this.route.params.subscribe(param => {
            this.editMode = param.id ? true : false;
            if (this.topicData && this.editMode) {       // change after finding a bug to only this.topicData
                this.loadTopic(this.topicData)
            }
        })
    }
    ngOnDestroy() {
        this.createTopic.reset();
        this.topicData.attachments = [];
        this.topicData = null;
        this.tags = [];
    }
}
