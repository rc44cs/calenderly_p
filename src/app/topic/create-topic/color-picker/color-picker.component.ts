import { AppState } from './../../../store/app.reducers';
import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
// import { EventEmitter } from 'protractor';
import { Event } from '@angular/router';
import { Store } from '@ngrx/store';

@Component({
  selector: 'color-picker',
  templateUrl: './color-picker.component.html',
  styleUrls: ['./color-picker.component.scss']
})
export class ColorPickerComponent implements OnInit {

  constructor(private store: Store<AppState>) { }
  @Input() color;
  @Output() setColorEvent: EventEmitter<any> = new EventEmitter();

  ngOnInit() {
    this.store.select('post').subscribe(state => {
      // this.color = state.currentTopic.color;
    })
  }
  setColor(color) {
    this.color = color;
    this.setColorEvent.emit(color);
  }
  colors = [
    {
      name: 'green'
    },
    {
      name: 'blue'
    },
    {
      name: 'red'
    },
    {
      name: 'orange'
    },
    {
      name: 'teal'
    },
    {
      name: 'cyan'
    },
    {
      name: 'blue-grey'
    },
    {
      name: 'purple'
    },
    {
      name: 'indigo'
    },
    {
      name: 'lime'
    }
  ]

}
