import { QuillModule } from 'ngx-quill';
import { EditorModule } from './../editor/editor.module';
import { CoreModule } from './../core/core.module';
import { ColorPickerComponent } from './create-topic/color-picker/color-picker.component';
import { TopicWatchersComponent } from './topic-view/topic-watchers/topic-watchers.component';
import { CreateTopicComponent } from './create-topic/create-topic.component';
import { NgModule } from '@angular/core';
import { PostService } from '../post/post.service';
import { TopicViewComponent } from './topic-view/topic-view.component';

@NgModule({
  imports: [
    CoreModule,
    EditorModule,
    QuillModule
  ],
  declarations: [
    TopicViewComponent,
    TopicWatchersComponent,
    ColorPickerComponent,
    CreateTopicComponent
  ],
  exports: [CreateTopicComponent, TopicViewComponent],
  providers: [PostService]
})

export class TopicModule { }