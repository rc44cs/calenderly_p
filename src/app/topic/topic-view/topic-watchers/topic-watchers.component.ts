import { Observable } from 'rxjs';
import { AppState } from './../../../store/app.reducers';
import { config } from './../../../providers/config';
import { Component, OnInit, Input, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { Store } from '@ngrx/store';
import { AutoUnsubscribe } from '../../../layout/auto-unsub';
@Component({
  selector: 'topic-watchers',
  templateUrl: './topic-watchers.component.html',
  styleUrls: ['./topic-watchers.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
@AutoUnsubscribe
export class TopicWatchersComponent implements OnInit {

  constructor(private store: Store<AppState>, private ref: ChangeDetectorRef) { }
  fallbackImg = config.images.fallbackImg;
  subscriptions=[];
  watchers: any[] = []
  ngOnInit() {
    this.subscriptions[0]=this.store.select('post').subscribe(state => {
      this.watchers = state.currentTopic.watchers;
      this.ref.markForCheck();
    })
  }
  ngOnDestroy() { }
}
