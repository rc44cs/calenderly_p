import { EventsService } from './../../providers/events.service';
import * as PostActions from './../../post/store/post.actions';
import { AppState } from './../../store/app.reducers';
import { SharedService } from './../../providers/shared.service';
import { config } from './../../providers/config';
import { Store } from '@ngrx/store';
import { PostService } from './../../post/post.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { AutoUnsubscribe } from '../../layout/auto-unsub';
declare var swal: any;
@Component({
  selector: 'app-topic-view',
  templateUrl: './topic-view.component.html',
  styleUrls: ['./topic-view.component.scss']
})
@AutoUnsubscribe
export class TopicViewComponent {
  images = config.images;
  constructor(private route: ActivatedRoute, private router: Router,
    public eventsService: EventsService,
    private postService: PostService, private sharedService: SharedService,
    private store: Store<AppState>) {

    this.store.select('main').subscribe(state => {
      this.members = state.project.members;
    });
  }

  post: any = { comments: [], watchers: [] };
  date;
  time;
  comments = [];
  commentBody;
  user: any;
  topic: any;
  subscriptions = [];
  uiState: any = { http: {} };
  statuses = [];
  members = [];
  changeStatus(e) {
    this.store.dispatch(new PostActions.UiHttp({
      watch: true
    }));
    this.store.dispatch(new PostActions.DragTopic({
      id: this.topic.id,
      prevStatus: this.post.status,
      data: {
        id: this.topic.id,
        status: this.statuses[Number(e.target.value)].id,
        version: this.post.version
      }
    }));
  }

  ngOnInit() {
    this.subscriptions[0] = this.store.select('post').subscribe(state => {
      console.log(state, 'state')
      this.uiState = state.uiState;
      this.topic = state.currentTopic;
    })

    this.subscriptions[1] = this.store.select('main').subscribe(state => {
      this.user = state.user;
    })


    this.subscriptions[2] = this.store.select('post').subscribe((state) => {
      this.statuses = state.statuses;
    })
  }

  watchTopic() {
    this.store.dispatch(new PostActions.UiHttp({
      watch: true
    }));
    this.store.dispatch(new PostActions.WatchTopic(this.topic))
  }

  onChangeAssigned(event) {
    this.topic.assigned_to = event;
    this.postService.updateAssignedTo(this.topic).subscribe(res => {
      if (res) {
        this.sharedService.notify('Topic updated successfully', 'success');
      }
    }, err => {
      this.sharedService.notify(err.error.version, 'error');
    });
    // this.store.dispatch(new PostActions.UiHttp({
    //   topic: true
    // }))
    // this.store.dispatch(new PostActions.UpdateTopic(this.topic))
  }


  deleteTopic(topic) {
    swal({
      title: 'Are you sure?',
      text: "You will not be able to recover this topic",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      reverseButtons: true,
      confirmButtonText: 'Delete'
    }).then((result) => {
      if (result.value) {
        this.store.dispatch(new PostActions.UiHttp({ calendar: true }));
        this.store.dispatch(new PostActions.DeleteTopic(topic.id));
        this.router.navigate(['/', 'calendar']);
      }
    })
  }

  deletePost(post) {
    swal({
      title: 'Are you sure?',
      text: "You will not be able to recover this post",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Delete',
      reverseButtons: true
    }).then((result) => {
      if (result.value) {
        this.store.dispatch(new PostActions.UiHttp({ topic: true }));
        this.store.dispatch(new PostActions.DeletePost(post.id));
        // this.router.navigate(['/', 'calendar'])
      }
    })
  }

  editPost(ref) {
    this.store.dispatch(new PostActions.UiHttp({ topic: true }));
    // this.store.dispatch(new MainActions.ChangeUiState({
    //   kanban: true
    // }))
    this.router.navigate(['/', 'post', ref])
  }



  deleteComment(data) {
    console.log(data, 'data')
    var post = data.post;
    var comment = data.comment;
    var index = data.index;
    this.store.dispatch(new PostActions.DeleteComment({ postId: post.id, commentId: comment.id, index: index }));
  }

  postComment(body) {
    if (body) {
      console.log(this.comments)
      this.subscriptions[2] = this.postService.updateTopic({ comment: body, version: 1, id: this.post.id }).subscribe((res: any) => {
        console.log(res)
        this.comments.push({
          user: {
            name: res.assigned_to_extra_info.username,
            photo: res.owner_extra_info.photo
          },
          comment: body
        })
        this.commentBody = ''
      }, er => {
        console.log(er)
      })
    }

  }

  ngOnDestroy() { }

}
