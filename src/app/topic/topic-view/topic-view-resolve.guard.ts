import * as PostActions from './../../post/store/post.actions';
import { AppState } from './../../store/app.reducers';
import { Store } from '@ngrx/store';
import { SharedService } from '../../providers/shared.service';
import { PostService } from '../../post/post.service';
import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Resolve } from '@angular/router';
import { Observable ,  forkJoin ,  of } from 'rxjs';
import { switchMap, map } from 'rxjs/operators';
@Injectable()
export class TopicViewResolveGuard implements Resolve<any> {
  constructor(private postService: PostService, private sharedService: SharedService,
    private store: Store<AppState>) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> {
    var observables = [];
    var watchers = [];
    var topicId = route.params.id;
    var topic;
    this.store.dispatch(new PostActions.UiHttp({calendar:true}));
    var comments;
    return this.postService.getTopic(topicId)
    .pipe(switchMap(t => {
      console.log(t)
      topic = t;
      topic.watchers.forEach(id => {
        observables.push(this.sharedService.getUserDetails(id))
      })
      if (observables.length) {
        return forkJoin(observables);
      }
      else {
        return Observable.create(observer => {
          observer.next([]);
          observer.complete();
        })
      }
    }),switchMap((results: any[]) => {
      console.log(results, 'results')
      watchers = results;
      return this.postService.getCommentsOnTopic(topic.id)
        .pipe(() => of(new PostActions.UiHttp({calendar:false})))
    }),map(c => {
      comments = c;
      var newTopic = {
        ...topic,
        comments: comments,
        watchers: watchers
      }
      this.store.dispatch(new PostActions.GetTopicSuccess(newTopic));
      this.store.dispatch(new PostActions.UiHttp({ calendar: false }));
      return {
        topic: topic,
        comments: comments,
        watchers: watchers
      }
    }))

  }
}
