import { config } from './../providers/config';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-page-header',
  templateUrl: './page-header.component.html',
  styleUrls: ['./page-header.component.scss']
})
export class PageHeaderComponent implements OnInit {
  images: any = config.images;

  constructor() { }

  ngOnInit() {
  }

}
