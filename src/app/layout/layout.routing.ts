import { TopicViewResolveGuard } from './../topic/topic-view/topic-view-resolve.guard';
import { TopicViewComponent } from './../topic/topic-view/topic-view.component';
import { LayoutResolveGuard } from './layout-resolve.guard';
import { PostViewComponent } from './../post/post-view/post-view.component';
import { AuthGuard } from './../providers/auth.guard';
import { Routes, RouterModule } from '@angular/router';
import { LayoutComponent } from './layout.component';
import { PostViewResolveGuard } from '../post/post-view/post-view-resolve.guard';

const LAYOUT_ROUTES: Routes = [
    {
        path: '', component: LayoutComponent,
        canActivate: [AuthGuard], 
        resolve:{
            layout:LayoutResolveGuard
        },
        children: [

            { path: '', redirectTo: 'calendar', pathMatch: 'full' },
            {
                path: 'calendar', loadChildren: '../calendar/calendar.module#CalendarModule',
            },
            {
                path: 'kanban', loadChildren: '../kanban/kanban.module#KanbanModule'
            },
            {
                path: 'settings', loadChildren: '../settings/settings.module#SettingsModule'
            },
            {
                path: 'post',
                children: [{
                    path: ':id',
                    component: PostViewComponent,
                    resolve:
                    {
                        post:PostViewResolveGuard
                    }
                }]
            },
            {
                path: 'topic',
                children: [{
                    path: ':id',
                    component: TopicViewComponent,
                    resolve:
                    {
                        topic:TopicViewResolveGuard
                    }
                }]
            }
        ]

    },
    {
        path: 'pages', loadChildren: '../pages/pages.module#PagesModule' 
    },
    {
        path: '**', redirectTo: 'calendar', pathMatch: 'full'
    }
];

export const LayoutRouting = RouterModule.forChild(LAYOUT_ROUTES);