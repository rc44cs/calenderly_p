import * as MainActions from './store/main.actions';
import { AppState } from './../store/app.reducers';
import { Router, NavigationEnd, ActivatedRouteSnapshot, ActivatedRoute } from '@angular/router';
import { SharedService } from '../providers/shared.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { AutoUnsubscribe } from './auto-unsub';
import { Location } from '@angular/common';
import { TourService } from 'ngx-tour-ngx-bootstrap';
import { Store, State } from '@ngrx/store';
import { EventsService } from '../providers/events.service';
import { environment } from '../../environments/environment';
import { ModalDirective } from '../../../node_modules/ngx-bootstrap';
declare var $: any;
@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./style.scss']
})
@AutoUnsubscribe
export class LayoutComponent implements OnInit {
  maTheme: string = this.sharedService.maTheme;
  hideBtn;
  script;
  user;
  date;
  time;
  status;
  subscriptions = [];
  visibleBackBtn = false;
  @ViewChild('createPost') createPost: ModalDirective;
  @ViewChild('createTopic') createTopic: ModalDirective;
  @ViewChild('addUserModal') addUserModal: ModalDirective;
  steps: any[] = [{
    anchorId: 'tour.1',
    content: 'Let us show you our home around so you know where to go and how to get things done easily 😃',
    placement: 'bottom',
    title: 'Welcome to Calendarly!',
    enableBackdrop: true,
    hasNavigation: true,
    hasBack: true,
    hasNext: true,
    showEnd: true
  },
  {
    anchorId: 'tour.2',
    content: 'Here you can see all your planned posts by publication date. This is always up to date! If you want to check the progress of a specific Post just click on it.',
    placement: 'bottom',
    title: 'This is your Calendar page',
    enableBackdrop: true,
    hasNavigation: true,
    hasBack: true,
    hasNext: true,
    showEnd: true
  },
  {
    anchorId: 'tour.3',
    content: 'You can create a Topic by clicking here. A Topic is a way to group Posts that are related, for example, a Topic can be a campaign',
    placement: 'bottom',
    title: 'Create Topic',
    enableBackdrop: true,
    hasNavigation: true,
    hasBack: true,
    hasNext: true,
    showEnd: true
  },
  // {
  //   anchorId: 'tour.4',
  //   content: 'You can save the Topic by clicking here.',
  //   placement: 'bottom',
  //   title: 'Create a Topic',
  //   enableBackdrop: true,
  //   hasNavigation: true,
  //   hasBack: true,
  //   hasNext: true,
  //   showEnd: true
  // },
  {
    anchorId: 'tour.5',
    content: 'You can create a post by clicking here.',
    placement: 'bottom',
    title: 'Create Post',
    enableBackdrop: true,
    hasNavigation: true,
    hasBack: true,
    hasNext: false,
    showEnd: true
  },
  {
    anchorId: 'tour.6',
    content: 'You can save the post by clicking here.cjhgjg',
    placement: 'top',
    title: 'Create Post',
    enableBackdrop: true,
    hasNavigation: true,
    hasBack: true,
    hasNext: false,
  
    showEnd: true
  }
];


  constructor(private sharedService: SharedService, private router: Router, private location: Location,
    public tourService: TourService, private store: Store<AppState>, private es: EventsService, private state: State<AppState>,
    private route: ActivatedRoute) {
    this.subscriptions[0] = sharedService.maThemeSubject.subscribe((value) => {
      this.maTheme = value;
    })
  }

  nextStep(step) {
    switch (step.anchorId) {
      case this.steps[0].anchorId:
        this.es.clickAddBtn.next(true);
    }
  }


  ngOnInit() {

    this.es.printScript.subscribe(script => {
      this.script = script;
    });

    this.store.select('main').subscribe(state => {
      this.user = state.user;
      this.script = state.uiState.gtmScript;
    });

    this.sharedService.notify('Welcome to Calendarly');

    this.router.events.subscribe((e: any) => {
      if (e instanceof NavigationEnd) {
        if (e.url.includes('/post') || e.url.includes('/topic')) {
          this.visibleBackBtn = true;
        } else {
          this.visibleBackBtn = false;
        }
      }
    })

    this.tourInitialize();
  }

  tourInitialize() {
    var w = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
    if (w > 767 && this.state.value.main.uiState.showTour) {
      // if (true) {
      this.tourService.initialize(this.steps);
      this.tourService.start();
    }

    this.es.hideTopic.subscribe(() => {
      this.createTopic.hide();
    });
    this.es.openPostSubject.subscribe((data?: any) => {
      if (data) {
        this.date = data.date;
        this.time = data.time;
        this.status = data.status;
      } else {
        this.date = null;
        this.time = null;
      }
      this.createPost.show();
    })

    this.es.openTopic.subscribe((data?: any) => {
      this.createTopic.show();
    })

    this.es.hidePostSubject.subscribe(() => {
      this.createPost.hide();
    })

    this.es.openAddMember.subscribe(() => {
      this.addUserModal.show();
    })

    this.es.hideAddMember.subscribe(() => {
      this.addUserModal.hide();
    })
  }

  goback() {
    this.router.navigate(['../'])
  }

  tourStart(event) {
    this.tourInitialize();
  }

  exit(type) {
    if (type === 'post') {
      this.es.hidePostSubject.next()
    }
    if (type === 'topic') {
      this.es.hideTopic.next();
    }
  }

  goBack() {
    console.log('go back', this.router.url)
    if (this.router.url !== '/calendar') {
      this.location.back();
    }
  }

  ngAfterViewInit(): void {
    $('#OzCNytY-1542091457585').css('left', 'auto');
  }

  endTour() {
    this.tourService.end();
    this.store.dispatch(new MainActions.ChangeUiState({
      showTour: false
    }));
  }

}
