export const ProjectTemplate = {
    "name": "Calendarly-Standard",
    "slug": "calendarly-standard",
    "default_owner_role": "product-owner",
    "description": "La pila de historias de usuario o \"product backlog\" en Scrum es una lista priorizada de funcionalidades, que contiene una breve descripción de todas las funciones y requisitos que debe cumplir el producto. Si utilizamos Scrum, no es necesario al inicio del proyecto dedicar tiempo y esfuerzo para tener todas las funcionalidades definidas y priorizadas, si no más bien definir un producto base y dejar que vaya creciendo con el paso del tiempo y las nuevas necesidades del cliente.",
    "is_backlog_activated": false,
    "is_wiki_activated": false,
    "is_issues_activated": false,
    "is_contact_activated": false,
    "is_kanban_activated": true,
    "points": [
        {
            "name": "?",
            "value": null,
            "order": 1
        },
        {
            "name": "0",
            "value": 0,
            "order": 2
        },
        {
            "name": "1/2",
            "value": 0.5,
            "order": 3
        },
        {
            "name": "1",
            "value": 1,
            "order": 4
        },
        {
            "name": "2",
            "value": 2,
            "order": 5
        },
        {
            "name": "3",
            "value": 3,
            "order": 6
        },
        {
            "name": "5",
            "value": 5,
            "order": 7
        },
        {
            "name": "8",
            "value": 8,
            "order": 8
        },
        {
            "name": "10",
            "value": 10,
            "order": 9
        },
        {
            "name": "13",
            "value": 13,
            "order": 10
        },
        {
            "name": "20",
            "value": 20,
            "order": 11
        },
        {
            "name": "40",
            "value": 40,
            "order": 12
        }
    ],
    "us_statuses": [
        {
            "wip_limit": null,
            "name": "New",
            "is_closed": false,
            "slug": "new",
            "is_archived": false,
            "color": "#999999",
            "order": 1
        },
        {
            "wip_limit": null,
            "name": "In progress",
            "is_closed": false,
            "slug": "in-progress",
            "is_archived": false,
            "color": "#ff9900",
            "order": 2
        },
        {
            "wip_limit": null,
            "name": "Ready",
            "is_closed": false,
            "slug": "ready",
            "is_archived": false,
            "color": "#ff8a84",
            "order": 3
        },
        {
            "wip_limit": null,
            "name": "Ready for approval",
            "is_closed": false,
            "slug": "ready-for-approval",
            "is_archived": false,
            "color": "#fcc000",
            "order": 4
        },
        {
            "wip_limit": null,
            "name": "Done",
            "is_closed": true,
            "slug": "done",
            "is_archived": false,
            "color": "#669900",
            "order": 5
        },
        {
            "wip_limit": null,
            "name": "Archived",
            "is_closed": true,
            "slug": "archived",
            "is_archived": true,
            "color": "#5c3566",
            "order": 6
        }
    ],
    "videoconferences": null,
    "severities": [
        {
            "name": "Wishlist",
            "color": "#666666",
            "order": 1
        },
        {
            "name": "Minor",
            "color": "#669933",
            "order": 2
        },
        {
            "name": "Normal",
            "color": "#0000FF",
            "order": 3
        },
        {
            "name": "Important",
            "color": "#FFA500",
            "order": 4
        },
        {
            "name": "Critical",
            "color": "#CC0000",
            "order": 5
        }
    ],
    "videoconferences_extra_data": "",
    "default_options": {
        "task_status": "New",
        "severity": "Normal",
        "issue_status": "New",
        "issue_type": "Bug",
        "epic_status": "New",
        "us_status": "New",
        "points": "?",
        "priority": "Normal"
    },
    "roles": [
        {
            "name": "Design",
            "slug": "design",
            "permissions": [
                "add_issue",
                "modify_issue",
                "delete_issue",
                "view_issues",
                "add_milestone",
                "modify_milestone",
                "delete_milestone",
                "view_milestones",
                "view_project",
                "add_task",
                "modify_task",
                "delete_task",
                "view_tasks",
                "add_us",
                "modify_us",
                "delete_us",
                "view_us",
                "add_wiki_page",
                "modify_wiki_page",
                "delete_wiki_page",
                "view_wiki_pages",
                "add_wiki_link",
                "delete_wiki_link",
                "view_wiki_links",
                "view_epics",
                "add_epic",
                "modify_epic",
                "delete_epic",
                "comment_epic",
                "comment_us",
                "comment_task",
                "comment_issue",
                "comment_wiki_page"
            ],
            "computable": true,
            "order": 10
        },
        {
            "name": "Copywriter",
            "slug": "copywriter",
            "permissions": [
                "add_issue",
                "modify_issue",
                "delete_issue",
                "view_issues",
                "add_milestone",
                "modify_milestone",
                "delete_milestone",
                "view_milestones",
                "view_project",
                "add_task",
                "modify_task",
                "delete_task",
                "view_tasks",
                "add_us",
                "modify_us",
                "delete_us",
                "view_us",
                "add_wiki_page",
                "modify_wiki_page",
                "delete_wiki_page",
                "view_wiki_pages",
                "add_wiki_link",
                "delete_wiki_link",
                "view_wiki_links",
                "view_epics",
                "add_epic",
                "modify_epic",
                "delete_epic",
                "comment_epic",
                "comment_us",
                "comment_task",
                "comment_issue",
                "comment_wiki_page"
            ],
            "computable": true,
            "order": 20
        },
        {
            "name": "Product Owner",
            "slug": "product-owner",
            "permissions": [
                "add_issue",
                "modify_issue",
                "delete_issue",
                "view_issues",
                "add_milestone",
                "modify_milestone",
                "delete_milestone",
                "view_milestones",
                "view_project",
                "add_task",
                "modify_task",
                "delete_task",
                "view_tasks",
                "add_us",
                "modify_us",
                "delete_us",
                "view_us",
                "add_wiki_page",
                "modify_wiki_page",
                "delete_wiki_page",
                "view_wiki_pages",
                "add_wiki_link",
                "delete_wiki_link",
                "view_wiki_links",
                "view_epics",
                "add_epic",
                "modify_epic",
                "delete_epic",
                "comment_epic",
                "comment_us",
                "comment_task",
                "comment_issue",
                "comment_wiki_page"
            ],
            "computable": false,
            "order": 30
        },
        {
            "name": "Stakeholder",
            "slug": "stakeholder",
            "permissions": [
                "add_issue",
                "modify_issue",
                "delete_issue",
                "view_issues",
                "view_milestones",
                "view_project",
                "view_tasks",
                "view_us",
                "modify_wiki_page",
                "view_wiki_pages",
                "add_wiki_link",
                "delete_wiki_link",
                "view_wiki_links",
                "view_epics",
                "comment_epic",
                "comment_us",
                "comment_task",
                "comment_issue",
                "comment_wiki_page"
            ],
            "computable": false,
            "order": 40
        }
    ],
    "issue_statuses": [
        {
            "name": "New",
            "slug": "new",
            "is_closed": false,
            "color": "#8C2318",
            "order": 1
        },
        {
            "name": "In progress",
            "slug": "in-progress",
            "is_closed": false,
            "color": "#5E8C6A",
            "order": 2
        },
        {
            "name": "Ready for test",
            "slug": "ready-for-test",
            "is_closed": true,
            "color": "#88A65E",
            "order": 3
        },
        {
            "name": "Closed",
            "slug": "closed",
            "is_closed": true,
            "color": "#BFB35A",
            "order": 4
        },
        {
            "name": "Needs Info",
            "slug": "needs-info",
            "is_closed": false,
            "color": "#89BAB4",
            "order": 5
        },
        {
            "name": "Rejected",
            "slug": "rejected",
            "is_closed": true,
            "color": "#CC0000",
            "order": 6
        },
        {
            "name": "Postponed",
            "slug": "posponed",
            "is_closed": false,
            "color": "#666666",
            "order": 7
        }
    ],
    "task_statuses": [
        {
            "name": "New",
            "slug": "new",
            "is_closed": false,
            "color": "#999999",
            "order": 1
        },
        {
            "name": "In progress",
            "slug": "in-progress",
            "is_closed": false,
            "color": "#ff9900",
            "order": 2
        },
        {
            "name": "Ready for test",
            "slug": "ready-for-test",
            "is_closed": true,
            "color": "#ffcc00",
            "order": 3
        },
        {
            "name": "Closed",
            "slug": "closed",
            "is_closed": true,
            "color": "#669900",
            "order": 4
        },
        {
            "name": "Needs Info",
            "slug": "needs-info",
            "is_closed": false,
            "color": "#999999",
            "order": 5
        }
    ],
    "priorities": [
        {
            "name": "Low",
            "color": "#666666",
            "order": 1
        },
        {
            "name": "Normal",
            "color": "#669933",
            "order": 3
        },
        {
            "name": "High",
            "color": "#CC0000",
            "order": 5
        }
    ],
    "epic_statuses": [
        {
            "name": "New",
            "slug": "new",
            "is_closed": false,
            "color": "#999999",
            "order": 1
        },
        {
            "name": "In progress",
            "slug": "in-progress",
            "is_closed": false,
            "color": "#ff9900",
            "order": 2
        },
        {
            "name": "Done",
            "slug": "done",
            "is_closed": true,
            "color": "#669900",
            "order": 3
        }
    ],
    "issue_types": [
        {
            "name": "Bug",
            "color": "#89BAB4",
            "order": 1
        },
        {
            "name": "Question",
            "color": "#ba89a8",
            "order": 2
        },
        {
            "name": "Enhancement",
            "color": "#89a8ba",
            "order": 3
        }
    ],
    "is_epics_activated": true
}