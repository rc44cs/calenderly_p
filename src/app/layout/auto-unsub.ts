import { isArray } from "util";

export function AutoUnsubscribe(constructor) {

    const original = constructor.prototype.ngOnDestroy;

    constructor.prototype.ngOnDestroy = function () {
        for (let prop in this) {
            const property = this[prop];
            if (isArray(property)) {
                // console.log(property,'property')
                property.forEach(prop => {
                    if (prop && prop.hasOwnProperty('unsubscribe') && (typeof prop.unsubscribe === "function")) {
                        prop.unsubscribe();
                    }
                })

            }
            else {
                if (property && property.hasOwnProperty('unsubscribe') && (typeof property.unsubscribe === "function")) {
                    property.unsubscribe();
                }
            }
        }
        original && typeof original === "function" && original.apply(this, arguments);
    };

}