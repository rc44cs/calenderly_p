import { Action } from "@ngrx/store";

export const AUTHENTICATE_USER = 'AUTHENTICATE_USER';
export const SIGN_OUT_USER = 'SIGN_OUT';
export const USER_UPDATED = 'USER_UPDATED';
export const SET_TOKEN = 'SET_TOKEN';
export const SHOW_LOADER = 'SHOW_LOADER'
export const NEW_NOTIFICATION = 'NEW_NOTIFICATION'
export const LOGOUT_USER = 'LOGOUT_USER'
export const LOGIN_USER = 'LOGIN_USER'
export const SET_AUTH_PARAMETERS = 'SET_AUTH_PARAMETERS'
export const SIGNUP_USER = 'SIGNUP_USER'
export const CHANGE_UI_STATE = 'CHANGE_UI_STATE'
export const READ_NOTIFICATION = 'READ_NOTIFICATION'
export const SHOW_SEARCH = 'SHOW_SEARCH'
export const SHOW_SEARCH_SUCCESS = 'SHOW_SEARCH_SUCCESS'
export const SHOW_SEARCH_ERROR = 'SHOW_SEARCH_ERROR';
export const SET_PROJECT_SUCCESS = 'SET_PROJECT_SUCCESS'
export const SET_PROJECT_TEMPLATE = 'SET_PROJECT_TEMPLATE';
export const SET_PROJECT_TEMPLATE_SUCCESS = 'SET_PROJECT_TEMPLATE_SUCCESS'

export const IS_ONLINE = 'IS_ONLINE';
export const IS_ONLINE_SUCCESS = 'IS_ONLINE_SUCCESS';
export const IS_ONLINE_ERROR = 'IS_ONLINE_ERROR';

export const FORGOT_PASSWORD = 'FORGOT_PASSWORD';
export const FORGOT_PASSWORD_SUCCESS = 'FORGOT_PASSWORD_SUCCESS';
export const FORGOT_PASSWORD_ERROR = 'FORGOT_PASSWORD_ERROR';

export const CHANGE_PASSWORD = 'CHANGE_PASSWORD'
export const CHANGE_PASSWORD_SUCCESS = 'CHANGE_PASSWORD_SUCCESS'

export const UPDATE_INVITATION = 'UPDATE_INVITATION'

export const UPDATE_PROJECT = 'UPDATE_PROJECT'
export const UPDATE_PROJECT_SUCCESS = 'UPDATE_PROJECT_SUCCESS'

export const UPDATE_USER_DETAILS = 'UPDATE_USER_DETAILS';

export class AuthenticateUser implements Action {
    readonly type = AUTHENTICATE_USER;
    constructor(public payload) { }
}

export class SignOutUser implements Action {
    readonly type = SIGN_OUT_USER;
    constructor(public payload) { }
}

export class ForgotPassword implements Action {
    readonly type = FORGOT_PASSWORD;
    constructor(public payload) { }
}

export class ForgotPasswordSuccess implements Action {
    readonly type = FORGOT_PASSWORD_SUCCESS;
    constructor(public payload) { }
}

export class ForgotPasswordError implements Action {
    readonly type = FORGOT_PASSWORD_ERROR;
    constructor(public payload) { }
}

export class UserUpdated implements Action {
    readonly type = USER_UPDATED;
    constructor(public payload) { }
}

export class SetToken implements Action {
    readonly type = SET_TOKEN;
    constructor(public payload) { }
}

export class LoginUser implements Action {
    readonly type = LOGIN_USER;
    constructor(public payload) { }
}

export class ShowLoader implements Action {
    readonly type = SHOW_LOADER;
    constructor(public payload) { }
}

export class NewNotification implements Action {
    readonly type = NEW_NOTIFICATION;
    constructor(public payload) { }
}

export class ShowSearch implements Action {
    readonly type = SHOW_SEARCH;
    constructor(public payload) { }
}


export class ReadNotification implements Action {
    readonly type = READ_NOTIFICATION;
    constructor(public payload) { }
}


export class SetAuthParameters implements Action {
    readonly type = SET_AUTH_PARAMETERS;
    constructor(public payload) { }
}

export class LogoutUser implements Action {
    readonly type = LOGOUT_USER;
}

export class SignupUser implements Action {
    readonly type = SIGNUP_USER;
    constructor(public payload) { }
}

export class SetProjectSuccess implements Action {
    readonly type = SET_PROJECT_SUCCESS;
    constructor(public payload) { }
}


export class ChangeUiState implements Action {
    readonly type = CHANGE_UI_STATE;
    constructor(public payload) { }
}

export class ShowSearchSuccess implements Action {
    readonly type = SHOW_SEARCH_SUCCESS;
    constructor(public payload) { }
}

export class ShowSearchError implements Action {
    readonly type = SHOW_SEARCH_ERROR;
    constructor(public payload) { }
}

export class SetProjectTemplate implements Action {
    readonly type = SET_PROJECT_TEMPLATE;
    constructor(public payload) { }
}

export class SetProjectTemplateSuccess implements Action {
    readonly type = SET_PROJECT_TEMPLATE_SUCCESS;
    constructor(public payload) { }
}


export class IsOnline implements Action {
    readonly type = IS_ONLINE;
    constructor(public payload) { }
}


export class ChangePassword implements Action {
    readonly type = CHANGE_PASSWORD;
    constructor(public payload) { }
}

export class ChangePasswordSuccess implements Action {
    readonly type = CHANGE_PASSWORD_SUCCESS;
    constructor(public payload) { }
}

export class UpdateInvitation implements Action {
    readonly type = UPDATE_INVITATION;
    constructor(public payload) { }
}

export class UpdateProject implements Action {
    readonly type = UPDATE_PROJECT;
    constructor(public payload) { }
}

export class UpdateProjectSuccess implements Action {
    readonly type = UPDATE_PROJECT_SUCCESS;
    constructor(public payload) { }
}

export type MainActions =
    AuthenticateUser |
    SignOutUser |
    ForgotPassword |
    UserUpdated |
    SetToken |
    ShowLoader |
    NewNotification |
    LogoutUser |
    LoginUser |
    SetAuthParameters |
    SignupUser |
    ChangeUiState |
    ReadNotification |
    ShowSearch |
    ShowSearchSuccess |
    ShowSearchError |
    SetProjectSuccess |
    SetProjectTemplate |
    SetProjectTemplateSuccess |
    IsOnline |
    ForgotPassword |
    ForgotPasswordSuccess |
    ForgotPasswordError |
    ChangePassword |
    ChangePasswordSuccess |
    UpdateInvitation |
    UpdateProject |
    UpdateProjectSuccess
