import { environment } from './../../../environments/environment';
import { AppState } from './../../store/app.reducers';
import { Store } from '@ngrx/store';
import { AuthService } from './../../providers/auth.service';
import * as MainActions from './../../layout/store/main.actions';
import { catchError, switchMap, map, finalize } from 'rxjs/operators';
import { forkJoin, Observable, of, empty } from 'rxjs';
import { DELETE_POST } from './../../kanban/store/kanban-actions';
import { SharedService } from './../../providers/shared.service';
import { Injectable } from '@angular/core';
import { PostService } from './../../post/post.service';
import { Actions, Effect } from '@ngrx/effects';
declare var $: any;
declare var dataLayer: any;
import { Router } from '@angular/router';

@Injectable()
export class MainEffects {
    constructor(private actions: Actions, private authService: AuthService,
        private sharedService: SharedService, private router: Router,
        private store: Store<AppState>) { }

    @Effect({
        dispatch: false
    }) loginUser$ = this.actions
        .ofType(MainActions.LOGIN_USER)
        .pipe(switchMap((action: MainActions.LoginUser) => {
            return this.authService.authenticateUser(action.payload)
                .pipe(map(data => {
                    // dataLayer.push(
                    //     {
                    //         'event': 'eventTracking',
                    //         'category': 'Authentication',
                    //         'action': 'login',
                    //         'label': 'success'
                    //     });
                    if (this.sharedService.initialPath) {
                        this.router.navigateByUrl(this.sharedService.initialPath);
                        this.sharedService.initialPath = null;
                    }
                    else {
                        this.router.navigate(['/', 'calendar']);
                    }
                }), catchError(() => {
                    // dataLayer.push({
                    //     'event': 'eventTracking',
                    //     'category': 'Authentication',
                    //     'action': 'login',
                    //     'label': 'fail'
                    // });
                    this.store.dispatch(new MainActions.ChangeUiState({ login: false }))
                    return empty()
                }));

        }))

    // @Effect({
    //     dispatch: false
    // }) updateProject$ = this.actions
    //     .ofType(MainActions.UPDATE_PROJECT)
    //     .pipe(switchMap((action: MainActions.UpdateProject) => {
    //         return this.sharedService.UpdateProject(action.payload)
    //             .pipe(map(data => {
    //                 return new MainActions.UpdateProjectSuccess(data);
    //             }), catchError(() => {
    //                 return empty()
    //             }));

    //     }))



    @Effect({
        dispatch: false
    }) signupUser$ = this.actions
        .ofType(MainActions.SIGNUP_USER)
        .pipe(switchMap((action: MainActions.SignupUser) => {
            console.log(action.payload)
            return this.authService.signupUser(action.payload)
                .pipe(map(data => {
                    this.router.navigate(['/', 'calendar']);
                }), catchError(er => {
                    if (er.status === 400) {
                        if (!er.error._error_message) {
                            this.sharedService.notify('There was an error, please try again', 'error');
                        } else {
                            this.sharedService.notify(er.error._error_message, 'error');
                        }

                    } else {
                        this.sharedService.notify('There was an error, please try again', 'error');
                    }

                    this.store.dispatch(new MainActions.ChangeUiState({ signup: false }))
                    return empty()
                })
                );
        }))

    @Effect({ dispatch: false }) forgotPass$ = this.actions
        .ofType(MainActions.FORGOT_PASSWORD)
        .pipe(switchMap((action: MainActions.ForgotPassword) => {
            return this.authService.forgotPassword(action.payload)
                .pipe(map(data => {
                    // dataLayer.push({
                    //     'event': 'eventTracking',
                    //     'category': 'Authentication',
                    //     'action': 'forgot password',
                    //     'label': 'success'
                    // });
                    this.sharedService.notify('Mail sent successfully!', 'success');
                }), catchError(er => {
                    // dataLayer.push({
                    //     'event': 'eventTracking',
                    //     'category': 'Authentication',
                    //     'action': 'forgot password',
                    //     'label': 'fail'
                    // });
                    this.sharedService.notify('Password recover email not sent. Please try again', 'error');
                    return empty()
                }), finalize(() => {
                    this.store.dispatch(new MainActions.ChangeUiState({ login: false }))

                })
                );
        }))

    @Effect({ dispatch: false }) changePassword$ = this.actions
        .ofType(MainActions.CHANGE_PASSWORD)
        .pipe(switchMap((action: MainActions.ChangePassword) => {
            return this.authService.changePassword(action.payload)
                .pipe(map(data => {
                    this.sharedService.notify('Password Changed Successfully!', 'success');
                    this.router.navigate(['/', 'pages', 'login']);
                }), catchError(er => {
                    this.sharedService.notify('Password recover email not sent, please, try again', 'error');
                    return empty()
                })
                );
        }))


}