import * as MainActions from './main.actions';
import { environment } from '../../../environments/environment';

export interface State {
    appId: string;
    authHeader: string;
    appState: string;
    isAuthenticated: boolean;
    storageStrategy: string;
    cypheredToken: string;
    invitation: any;
    isOnline;
    auth_code: string;
    user: any;
    uiState: any;
    notifications: any[];
    project: any;
    template: any;
}

export const initialState: State = {
    appId: environment.appId,
    authHeader: 'Bearer',
    isAuthenticated: false,
    cypheredToken: null,
    invitation: {},
    auth_code: null,
    template: undefined,
    appState: 'random-state',
    storageStrategy: localStorage.getItem('storageStrategy') ? localStorage.getItem('storageStrategy') : 'sessionStorage',
    user: {},
    project: {
        roles: []
    },
    uiState: {
        showLoader: false,
        autoClose: true,
        gtmScript: '',
        login: false,
        signup: false,
        header: {
            search: false,
            addBtn: {
                show: false
            }
        },
        kanban: false,
        addItem: false,
        showTour: false,
        cameFromLogin: false
    },
    notifications: [],
    isOnline: navigator.onLine
}

export function MainReducers(state = initialState, action: MainActions.MainActions) {

    switch (action.type) {
        case MainActions.AUTHENTICATE_USER:
            return {
                ...state,
                isAuthenticated: action.payload.isAuthenticated,
                user: action.payload.user
            }
        case MainActions.CHANGE_UI_STATE:
            return {
                ...state,
                uiState: { ...state.uiState, ...action.payload }
            }

        case MainActions.USER_UPDATED:
            return {
                ...state,
                user: action.payload
            }


        case MainActions.SET_PROJECT_SUCCESS:
            return {
                ...state,
                project: action.payload
            }
        case MainActions.UPDATE_INVITATION:
            return {
                ...state,
                invitation: action.payload
            }

        case MainActions.IS_ONLINE:
            return {
                ...state,
                isOnline: action.payload
            }

        case MainActions.READ_NOTIFICATION:
            var notification = action.payload;
            var notifications = [...state.notifications].filter(n => n.id !== notification.id)
            return {
                ...state,
                notifications: [...notifications]
            }

        case MainActions.SHOW_LOADER:
            return {
                ...state,
                uiState: { ...state.uiState, showLoader: action.payload }
            }

        case MainActions.SET_TOKEN:
            window[this.container.storageStrategy].setItem('cypheredToken', action.payload);
            return {
                ...state,
                cypheredToken: action.payload
            }

        case MainActions.LOGOUT_USER:
            return {
                ...state,
                ...initialState
            }

        case MainActions.SET_AUTH_PARAMETERS:
            var data = action.payload;
            return {
                ...state,
                ...action.payload
            }

        case MainActions.SET_PROJECT_TEMPLATE_SUCCESS:
            var template = action.payload;
            return {
                ...state,
                template: template
            }

        case MainActions.NEW_NOTIFICATION:
            var notifications = [...state.notifications];
            var notification = action.payload;
            notification.id = Date.now();
            notifications.push(notification);

            return {
                ...state,
                cypheredToken: action.payload,
                notifications: notifications
            }

        default:
            return {
                ...state
            }
    }

}