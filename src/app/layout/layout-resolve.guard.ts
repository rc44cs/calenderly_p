import { map, switchMap } from 'rxjs/operators';
import * as MainActions from './store/main.actions';
import * as PostActions from './../post/store/post.actions';
import { Store } from '@ngrx/store';
import { AppState } from './../store/app.reducers';
import { SharedService } from './../providers/shared.service';
import { PostService } from './../post/post.service';
import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Resolve } from '@angular/router';
import { Observable, forkJoin } from 'rxjs';
@Injectable()
export class LayoutResolveGuard implements Resolve<any> {
  constructor(private postService: PostService,
    private sharedService: SharedService, private store: Store<AppState>) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> {
    var observables = [];
    return this.sharedService.getProjects()
      .pipe(switchMap(project => {
        this.store.dispatch(new MainActions.SetProjectSuccess(project));
        observables[0] = this.postService.getPosts()
        observables[1] = this.sharedService.getPostStatuses();
        observables[2] = this.sharedService.getProjectTemplate();
        return forkJoin(observables).pipe(map((results: any[]) => {
          var posts = results[0];
          var statuses = results[1];
          var template = results[2];
          statuses.forEach(o => {
            o.data = [];
            posts.forEach(p => {
              if (p.status === o.id) {
                o.data.push(p);
              }
            })
          })
          this.store.dispatch(new PostActions.SetStatuses(statuses));
          this.store.dispatch(new PostActions.GetPostsSuccess(posts));
          this.store.dispatch(new MainActions.SetProjectTemplateSuccess(template));
          this.store.dispatch(new MainActions.ChangeUiState({ login: false }));
          this.store.dispatch(new MainActions.ChangeUiState({ signup: false }))

          return {
            posts: posts,
            statuses: statuses
          }
        }));
      }))

  }
}
