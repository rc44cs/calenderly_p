import { config } from './../../providers/config';
import * as MainActions from './../store/main.actions';
import { Store } from '@ngrx/store';
import { AppState } from './../../store/app.reducers';
import { SharedService } from './../../providers/shared.service';
import { AuthService } from './../../providers/auth.service';
import { EventsService } from './../../providers/events.service';
import { Component, OnInit, OnDestroy, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { AutoUnsubscribe } from '../auto-unsub';

@Component({
    selector: 'app-navigation',
    templateUrl: './navigation.component.html',
    styleUrls: ['./navigation.component.scss'],
    // animations: [
    //     trigger('toggleHeight', [
    //         state('inactive', style({
    //             height: '0',
    //             opacity: '0'
    //         })),
    //         state('active', style({
    //             height: '*',
    //             opacity: '1'
    //         })),
    //         transition('inactive => active', animate('200ms ease-in')),
    //         transition('active => inactive', animate('200ms ease-out'))
    //     ])
    // ]
})

@AutoUnsubscribe
export class NavigationComponent implements OnInit, OnDestroy {
    sidebarVisible: boolean;
    user;
    fallbackImg = config.images.fallbackImg;
    subscriptions = [];
    @Output() tourStart = new EventEmitter();
    ngOnInit() {
        this.getUser();
        this.userUpdated();

        this.subscriptions[0] = this.sharedService.sidebarVisibilitySubject.subscribe((value) => {
            this.sidebarVisible = value;
        });
    }
    getUser() {
        this.subscriptions[2] = this.authService.getUserDetails().subscribe(user => {
            this.user = user;
        });
    }

    constructor(private sharedService: SharedService, private router: Router,
        private authService: AuthService, private store: Store<AppState>,
        private eventsService: EventsService) { }

    logout() {
        this.eventsService.exitUser.next();
    }

    onTourStart() {
        this.store.dispatch(new MainActions.ChangeUiState({
            showTour: true
        }));
        this.tourStart.emit();
    }

    userUpdated() {
        this.subscriptions[1] = this.store.select('main').subscribe(state => {
            this.user = state.user;
        });
    }



    ngOnDestroy() {

    }
}
