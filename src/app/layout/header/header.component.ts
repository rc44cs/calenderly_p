import { TourService } from 'ngx-tour-ngx-bootstrap';
import { config } from './../../providers/config';
import * as MainActions from './../store/main.actions';
import { Router } from '@angular/router';
import { AppState } from './../../store/app.reducers';
import { Store, State } from '@ngrx/store';
import { EventsService } from './../../providers/events.service';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { SharedService } from './../../providers/shared.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { AutoUnsubscribe } from '../auto-unsub';
declare var $: any
declare var swal: any;

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: [
    './header.component.scss'
  ]
})
@AutoUnsubscribe
export class HeaderComponent implements OnInit {
  messagesData: Array<any>;
  tasksData: Array<any>;
  maThemeModel: string = 'green';
  project;
  date;
  roles = [];
  time;
  images: any = config.images;
  subscriptions = [];
  user: any = {};
  template;
  status;
  notifications = [];
  @ViewChild('drpdwn') drpdwn: ModalDirective
  // @ViewChild('createTopic') createTopic: ModalDirective
  @ViewChild('searchActive') searchActive: ModalDirective
  showSearch = false;
  isAuthenticated = false;

  constructor(private sharedService: SharedService,
    public eventsService: EventsService, private store: Store<AppState>,
    public ts: TourService,
    private router: Router, private state: State<AppState>) {
    this.subscriptions[2] = sharedService.maThemeSubject.subscribe((value) => {
      this.maThemeModel = value
    })
  }

  setTheme() {
    this.sharedService.setTheme(this.maThemeModel)
  }


  toggleSearch() {
    this.store.dispatch(new MainActions.ChangeUiState({
      header: {
        search: !this.showSearch
      }
    }))
  }

  readNotification(item) {
    this.router.navigate(['/', item.type, item.item.ref]);
    this.store.dispatch(new MainActions.ReadNotification(item));
  }

  createItem(type) {
    if (type === 'post') {
      this.sharedService.createPostSubject.next();
    }
    if (type == 'topic') {
      this.sharedService.createTopicSubject.next();
    }
  }

  exit(type) {
    if (type === 'post') {
      this.eventsService.hidePostSubject.next()
    }
    if (type === 'topic') {
      this.eventsService.hideTopic.next();
    }
  }

  addMember() {
    this.user.role = Number(this.user.role)
    this.subscriptions[1] = this.sharedService.addMember({ project: this.state.value.main.project.id, ...this.user }).subscribe(res => {
      this.sharedService.notify('Member added Successfully')
    })
  }
  showTour;
  uiState;
  ngOnInit() {
    this.project = this.state.value.main.project;
    this.subscriptions[0] = this.store.select('main').subscribe(state => {
      this.notifications = state.notifications;
      this.uiState = state.uiState;
      this.user = state.user;
      this.showSearch = state.uiState.header.search;
      this.template = state.template;
      this.isAuthenticated = state.isAuthenticated;
    })

    this.store.select('main').subscribe(state => {
      this.showTour = state.uiState.showTour;
    })
  }

  gotonext() {
    console.log(this.showTour, 'show tour');
    if (this.showTour) {
      this.ts.next();
    }
    else {
      this.ts.end();
    }
  }

  ngOnDestroy() {

  }
}