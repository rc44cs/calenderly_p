import * as MainActions from './../../store/main.actions';
import { AppState } from './../../../store/app.reducers';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { SharedService } from './../../../providers/shared.service';
import { Component, OnInit } from '@angular/core';
import { isArray } from 'util';
import { Store } from '@ngrx/store';

@Component({
  selector: 'header-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})

export class SearchComponent implements OnInit {


  searchActive: boolean = false;
  searchValue: string = '';
  searchFocused: boolean = false;

  closeSearch() {
    // this.searchActive = false; // Close the search block
    this.store.dispatch(new MainActions.ChangeUiState({ header: { search: false } }))
    this.searchValue = null; // Empty the search field
    this.searchFocused = false;
  }
  searchObservable: Observable<any>;
  posts = [];
  topics = [];
  response = [];
  textBody = '';

  constructor(private sharedService: SharedService, private router: Router,
    private store: Store<AppState>) { }


  mapSearchResponse(obj) {
    Object.keys(obj).forEach(key => {
      if (isArray(obj[key])) {
        obj[key].forEach(o => {
          o.type = key;
          this.response.push(o);
        })
      }
    })
  }

  searchText(text) {
    this.searchObservable = this.sharedService.searchtext(text)
  }

  selectOption(item) {
    console.log(item);
    if (item.item.type === 'Posts') {
      this.router.navigate(['/', 'post', item.item.ref])
    }
    if (item.item.type === 'Topics') {
      this.router.navigate(['/', 'topic', item.item.ref])
    }
  }

  showSearch;
  ngOnInit() {
    this.searchObservable = this.sharedService.searchtext('');
    this.store.select('main').subscribe(state => {
      this.showSearch = state.uiState.header.search;
    })
  }
}