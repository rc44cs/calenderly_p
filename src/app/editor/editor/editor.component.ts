import { Subject } from 'rxjs';
import { Component, OnInit, ViewChild, ElementRef, Input } from '@angular/core';
import { PostService } from '../../post/post.service';
declare var $:any;

@Component({
  selector: 'app-editor',
  templateUrl: './editor.component.html',
  styleUrls: ['./editor.component.scss']
})
export class EditorComponent implements OnInit {
  constructor(private postService:PostService) { }
  @ViewChild('editor') editor:ElementRef;
@Input() initialHtml;

  ngOnInit() {
    $(this.editor.nativeElement).summernote({
      height: 150
  });

  $(this.editor.nativeElement).on('summernote.change',(contents, $editable)=>{
    // console.log('onChange:', $editable);
    this.postService.update$.next($editable);
})
  }

  ngOnChanges(change)
  {
    console.log(change,'hahah change')
    if(!change.isFirst)
    { 
      $(this.editor.nativeElement).summernote('code',change.initialHtml.currentValue);
    }
  }

}
