import * as MainActions from './../../layout/store/main.actions';
import { catchError, switchMap, map, finalize } from 'rxjs/operators';
import * as PostActions from './post.actions';
import { forkJoin, Observable, of, empty } from 'rxjs';
import { DELETE_POST } from './../../kanban/store/kanban-actions';
import { SharedService } from './../../providers/shared.service';
import { Injectable } from '@angular/core';
import { PostService } from './../../post/post.service';
import { Actions, Effect } from '@ngrx/effects';
import { AppState } from '../../store/app.reducers';
import { State, Store } from '@ngrx/store';
import { NgxSpinnerService } from 'ngx-spinner';
declare var dataLayer: any;
declare var ga: any;


@Injectable()
export class PostEffects {
    constructor(private actions: Actions, private postService: PostService, private spinner: NgxSpinnerService,
        private sharedService: SharedService, private store: Store<AppState>,
        private state: State<AppState>) { }

    @Effect() getPosts$ = this.actions
        .ofType(PostActions.GET_POSTS)
        .pipe(switchMap((action: PostActions.GetPosts) => {
            return this.postService.getPosts()
                .pipe(map(posts => {
                    return new PostActions.GetPostsSuccess(posts)
                }),
                    catchError(er => empty()), finalize(() => {
                        this.store.dispatch(new MainActions.ShowLoader(false))
                    }));
        }))

    @Effect() getComments$ = this.actions
        .ofType(PostActions.GET_COMMENTS)
        .pipe(switchMap((action: PostActions.GetComments) => {
            return this.postService.getComments(action.payload)
                .pipe(map(comments => {
                    return new PostActions.GetCommentsSuccess(comments)
                }),
                    catchError(() => empty())
                    , finalize(() => {
                        this.store.dispatch(new PostActions.UiHttp({
                            comment: false
                        }))
                    }));
        }))


    @Effect() createPost = this.actions.ofType(PostActions.CREATE_POST)
        .pipe(map((action: PostActions.CreatePost) => action.payload), switchMap(postData => {
            console.log(postData, 'postdata')
            return this.postService.createPost(postData)
                .pipe(switchMap((post: any) => {
                    console.log(dataLayer, 'aasd')
                    this.sharedService.notify('Post Created Successfully', 'success');
                    dataLayer.push({
                        'event': 'eventTracking',
                        'category': 'Post',
                        'action': 'create button',
                        'label': 'success'
                    });
                    return [
                        new PostActions.CreatePostSuccess(this.postService.mapPostResponse(post)),
                        new MainActions.NewNotification({ body: `${post.subject} has been created successfully`, item: post, type: 'post' },
                        )]
                }), catchError((er) => {
                    dataLayer.push({
                        'event': 'eventTracking',
                        'category': 'Post',
                        'action': 'create button',
                        'label': 'fail'
                    });
                    this.sharedService.notify('Post was not created!', 'error')
                    return empty();
                }), finalize(() => {
                    this.store.dispatch(new PostActions.UiHttp({ calendar: false }));
                    this.store.dispatch(new PostActions.UiHttp({ topic: false }));
                    this.store.dispatch(new MainActions.ChangeUiState({
                        kanban: false
                    }))
                }))
        }))


    @Effect() deletePost = this.actions.ofType(PostActions.DELETE_POST)
        .pipe(map((action: PostActions.DeletePost) => action.payload), switchMap(postId => {
            console.log(postId, 'postdata')
            return this.postService.deletePost(postId).pipe(map(postId => {
                this.sharedService.notify('Post Deleted Successfully', 'success');
                return new PostActions.DeletePostSuccess(postId)
            }),
                catchError((er) => {
                    this.sharedService.notify('Post was not deleted!', 'error')
                    return empty();
                }), finalize(() => {
                    this.store.dispatch(new PostActions.UiHttp({ calendar: false }));
                    this.store.dispatch(new PostActions.UiHttp({ kanban: false }))
                    this.store.dispatch(new PostActions.UiHttp({ topic: false }));
                    this.store.dispatch(new MainActions.ChangeUiState({
                        kanban: false
                    }))
                }))
        }))


    @Effect() deleteTopic = this.actions.ofType(PostActions.DELETE_TOPIC)
        .pipe(map((action: PostActions.DeleteTopic) => action.payload), switchMap(topicId => {
            console.log(topicId, 'postdata')
            return this.postService.deleteTopic(topicId)
                .pipe(map(topicId => {
                    this.sharedService.notify('Topic Deleted Successfully', 'success');
                    return new PostActions.DeleteTopicSuccess(topicId)
                }),
                    catchError((er) => {
                        this.sharedService.notify('Topic was not deleted!', 'error')
                        return empty();
                    }), finalize(() => {
                        this.store.dispatch(new PostActions.UiHttp({ calendar: false }));
                        this.store.dispatch(new PostActions.UiHttp({ topic: false }));
                    }));
        }))

    @Effect() createTopic = this.actions.ofType(PostActions.CREATE_TOPIC)
        .pipe(map((action: PostActions.CreateTopic) => action.payload), switchMap(topicData => {
            return this.postService.createTopic(topicData)
                .pipe(switchMap((topic: any) => {
                    dataLayer.push({
                        'event': 'eventTracking',
                        'category': 'Topic',
                        'action': 'create',
                        'label': 'success'
                    });
                    this.sharedService.notify('Topic Created Successfully', 'success')
                    console.log(topic, 'aasd')
                    return [{
                        type: PostActions.CREATE_TOPIC_SUCCESS,
                        payload: topic
                    }, new MainActions.NewNotification({ body: `${topic.subject} has been created successfully`, item: topic, type: 'topic' })]
                }),
                    catchError((er) => {
                        dataLayer.push({
                            'event': 'eventTracking',
                            'category': 'Topic',
                            'action': 'create',
                            'label': 'fail'
                        });
                        this.sharedService.notify('Topic was not created!', 'error')
                        return empty();
                    }), finalize(() => {
                        this.store.dispatch(new PostActions.UiHttp({ calendar: false }));
                    }));
        }))

    @Effect() dragPost = this.actions.ofType(PostActions.DRAG_POST)
        .pipe(switchMap((action: PostActions.DragPost) => {
            this.store.dispatch(new MainActions.ChangeUiState({
                kanban: true
            }));
            return this.postService.dragPost(action.payload.id, action.payload.data, action.payload.prevStatus)
                .pipe(map(post => {

                    this.sharedService.notify('Post Status Updated', 'success')
                    console.log(post)
                    return {
                        type: PostActions.DRAG_POST_SUCCESS,
                        payload: post
                    }
                }),
                    catchError((er) => {
                        this.sharedService.notify('Post Status was not updated!', 'error')
                        return empty();
                    }), finalize(() => {
                        this.store.dispatch(new MainActions.ChangeUiState({
                            kanban: false
                        }));
                        this.store.dispatch(new PostActions.UiHttp({
                            watch: false
                        }))
                    }))
        }))

    @Effect() dragTopic = this.actions.ofType(PostActions.DRAG_TOPIC)
        .pipe(switchMap((action: PostActions.DragTopic) => {
            return this.postService.dragTopic(action.payload.id, action.payload.data, action.payload.prevStatus)
                .pipe(map(post => {
                    this.sharedService.notify('Topic Status Updated', 'success')
                    console.log(post)
                    return {
                        type: PostActions.DRAG_POST_SUCCESS,
                        payload: post
                    }
                }),
                    catchError((er) => {
                        this.sharedService.notify('Topic Status was not updated!', 'error')
                        return empty();
                    }), finalize(() => {
                        this.store.dispatch(new MainActions.ChangeUiState({
                            kanban: false
                        }));
                        this.store.dispatch(new PostActions.UiHttp({
                            watch: false
                        }))
                    }))
        }))

    @Effect() addComment = this.actions.ofType(PostActions.ADD_COMMENT)
        .pipe(switchMap((action: PostActions.AddComment) => {
            var comment = action.payload;
            console.log(comment, 'payload')
            // delete comment.id;
            return this.postService.updatePost(comment)
                .pipe(map(post => post), map((post: any) => {
                    dataLayer.push({
                        'event': 'eventTracking',
                        'category': 'Post',
                        'action': 'new comment',
                        'label': 'success'
                    });
                    console.log(post, 'reessult')
                    return new PostActions.GetComments(post.id)
                }),
                    catchError((er) => {
                        dataLayer.push({
                            'event': 'eventTracking',
                            'category': 'Post',
                            'action': 'new comment',
                            'label': 'fail'
                        });
                        this.sharedService.notify('Comment was not added!', 'error')
                        return empty();
                    }), finalize(() => {
                        this.store.dispatch(new PostActions.UiHttp({
                            comment: false
                        }));
                    }))
        }))

    @Effect() updateTopic = this.actions.ofType(PostActions.UPDATE_TOPIC)
        .pipe(switchMap((action: PostActions.UpdateTopic) =>
            this.postService.updateTopic(action.payload)
                .pipe(map(topic => {
                    dataLayer.push({
                        'event': 'eventTracking',
                        'category': 'Topic',
                        'action': 'update',
                        'label': 'success'
                    });
                    console.log(topic, 'came');
                    this.sharedService.notify('Topic updated!', 'success');
                    return new PostActions.UpdateTopicSuccess(topic)
                }),
                    catchError((er) => {
                        dataLayer.push({
                            'event': 'eventTracking',
                            'category': 'Topic',
                            'action': 'update',
                            'label': 'fail'
                        });
                        this.sharedService.notify('Topic was not updated!', 'error')
                        return empty();
                    }), finalize(() => {
                        this.store.dispatch(new PostActions.UiHttp({ topic: false }));
                    }))))

    @Effect() deleteComment = this.actions.ofType(PostActions.DELETE_COMMENT)
        .pipe(switchMap((action: PostActions.DeleteComment) =>
            this.postService.deleteComment(action.payload)
                .pipe(map(index => {
                    this.sharedService.notify('Comment Deleted!', 'success');
                    return new PostActions.DeleteCommentSuccess(index)
                }),
                    catchError((er) => {
                        this.sharedService.notify('Comment was not deleted!', 'error');
                        this.store.dispatch(new PostActions.DeleteCommentError());
                        return empty();
                    }), finalize(() => {
                        this.store.dispatch(new PostActions.UiHttp({ comment: false }));
                    }))))

    @Effect() watchPost = this.actions.ofType(PostActions.WATCH_POST)
        .pipe(switchMap((action: PostActions.WatchPost) => {
            return this.postService.toggleWatchPost(action.payload)
                .pipe(map(user => {
                    console.log(user)
                    return new PostActions.WatchPostSuccess(user)
                }),
                    catchError((er) => {
                        this.sharedService.notify('Watcher list not updated!', 'error');
                        return empty();
                    }), finalize(() => {
                        this.store.dispatch(new PostActions.UiHttp({
                            watch: false
                        }));
                    }))
        }))

    @Effect() watchTopic = this.actions.ofType(PostActions.WATCH_TOPIC)
        .pipe(switchMap((action: PostActions.WatchTopic) => {
            return this.postService.toggleWatchTopic(action.payload)
                .pipe(map(user => {
                    console.log(user)
                    return new PostActions.WatchTopicSuccess(user)
                }),
                    catchError((er) => {
                        this.sharedService.notify('Watcher list not updated!', 'error');
                        return empty();
                    }), finalize(() => {
                        this.store.dispatch(new PostActions.UiHttp({
                            watch: false
                        }));
                    }))
        }))

    @Effect() addAttachment = this.actions.ofType(PostActions.ADD_ATTACHMENT)
        .pipe(map((action: PostActions.AddAttachment) => action.payload), map(data => { new PostActions.UiHttp({ attachments: true }); return data; })
            , switchMap(data => {
                this.spinner.show();
                return this.postService.createAttachment(data)
                    .pipe(map(file => {
                        dataLayer.push({
                            'event': 'eventTracking',
                            'category': 'Post',
                            'action': 'attachment',
                            'label': 'success',
                            'value': data.size
                        });
                        console.log(file);
                        this.spinner.hide();
                        this.sharedService.notify('Attachment added Successfully', 'success');
                        return new PostActions.AddAttachmentSuccess(file);
                    }),
                        catchError((er) => {
                            this.spinner.hide();
                            this.sharedService.notify(er.error._error_message , 'error');
                            dataLayer.push({
                                'event': 'eventTracking',
                                'category': 'Post',
                                'action': 'attachment',
                                'label': 'error'
                            });
                            // this.sharedService.notify('Attachment not added!', 'error');
                            return empty();
                        }), finalize(() => {
                            this.store.dispatch(new PostActions.UiHttp({ attachments: false }));
                        }));
            }))

    @Effect() deleteAttachment = this.actions.ofType(PostActions.DELETE_ATTACHMENT)
        .pipe(map((action: PostActions.DeleteAttachment) => action.payload), map((data) => { new PostActions.UiHttp({ attachments: true }); return data })
            , switchMap(data => {
                console.log(data);
                return this.postService.deleteAttachment(data.file, data.index)
                    .pipe(map(index => {
                        console.log(index);
                        this.sharedService.notify('Attachment deleted Successfully', 'success');
                        return new PostActions.DeleteAttachmentSuccess(index)
                    }),
                        catchError((er) => {
                            this.sharedService.notify('Attachment not deleted!', 'error');
                            return empty();
                        }), finalize(() => {
                            this.store.dispatch(new PostActions.UiHttp({ attachments: false }));
                        }));
            }))

    @Effect() updateAttachment = this.actions.ofType(PostActions.UPDATE_ATTACHMENT)
        .pipe(map((action: PostActions.UpdateAttachment) => action.payload), switchMap(data => {
            console.log(data);
            return this.postService.updateAttachment(data)
                .pipe(map(file => {
                    this.sharedService.notify('Attachment updated Successfully', 'success');
                    console.log(file, 'file')
                    return new PostActions.UpdateAttachmentSuccess(file)
                }),
                    catchError((er) => {
                        this.sharedService.notify('Attachment not updated!', 'error');
                        return empty();
                    }), finalize(() => {
                        this.store.dispatch(new PostActions.UiHttp({ attachments: false }));
                    }));
        })
        )


    @Effect() updatePost = this.actions.ofType(PostActions.UPDATE_POST)
        .pipe(switchMap((action: PostActions.UpdatePost) => {
            return this.postService.updatePost(action.payload)
                .pipe(map(post => {
                    dataLayer.push({
                        'event': 'eventTracking',
                        'category': 'Post',
                        'action': 'update',
                        'label': 'success'
                    });
                    console.log('it came')
                    this.sharedService.notify('Post updated Successfully', 'success')
                    console.log(post)
                    return new PostActions.UpdatePostSuccess(post)
                }),
                    catchError((er) => {
                        dataLayer.push({
                            'event': 'eventTracking',
                            'category': 'Post',
                            'action': 'update',
                            'label': 'fail'
                        });
                        this.sharedService.notify('Post was not updated!', 'error');
                        action.payload.loading = false;
                        return of(new PostActions.UpdatePostSuccess(action.payload));
                    }), finalize(() => {
                        this.store.dispatch(new MainActions.ChangeUiState({
                            kanban: false
                        }));
                        this.store.dispatch(new MainActions.ShowLoader(false));
                    }))
        }))

    @Effect() editComment = this.actions.ofType(PostActions.UPDATE_COMMENT)
        .pipe(switchMap((action: PostActions.UpdateComment) => {
            return this.postService.editComment(action.payload.postId, action.payload.comment)
                .pipe(map(comment => {
                    dataLayer.push({
                        'event': 'eventTracking',
                        'category': 'Post',
                        'action': 'edit comment',
                        'label': 'success'
                    });
                    this.sharedService.notify('Comment updated Successfully', 'success')
                    console.log(comment)
                    return new PostActions.UpdateCommentSuccess(comment)
                }),
                    catchError((er) => {
                        dataLayer.push({
                            'event': 'eventTracking',
                            'category': 'Post',
                            'action': 'edit comment',
                            'label': 'fail'
                        });
                        this.store.dispatch(new PostActions.UpdateCommentError())
                        this.sharedService.notify('Post was not updated!', 'error');
                        return empty();
                    }), finalize(() => {
                        this.store.dispatch(new MainActions.ChangeUiState({
                            kanban: false
                        }));
                        this.store.dispatch(new MainActions.ShowLoader(false));
                    }))

        }))

}