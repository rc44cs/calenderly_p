import * as PostActions from './post.actions';
import * as moment from 'moment';

export interface State {
    posts: any[];
    topics: any[];
    statuses: any[];
    currentPost: any;
    currentTopicPosts: any[];
    currentTopic: any;
    uiState: any;
}

export const initialState: State = {
    posts: [],
    topics: [],
    statuses: [],
    currentPost: {
        attachments: []
    },
    currentTopic: {
        attachments: []
    },
    currentTopicPosts: [],
    uiState: {
        http: {
            watch: false,
            comment: false,
            attachments: false,
            calendar: false,
            topic: false
        }
    }
}

export function PostReducer(state = initialState, action: PostActions.PostActions) {
    switch (action.type) {
        case PostActions.CREATE_POST_SUCCESS:
            var statuses = [...state.statuses];
            var post = action.payload;
            var topic = { ...state.currentTopic }
            statuses.forEach(o => {
                if (o.id === post.status) {
                    o.data.push(post)
                }
            })
            console.log(post, 'created!');
            post.topics.forEach(e => {
                if (e.id == topic.id) {
                    topic.topicPosts.push(topic);
                }
            })
            return { ...state, posts: [...state.posts, post], statuses: statuses, currentTopic: topic };

        case PostActions.DRAG_POST_SUCCESS:
            var prevPost = state.currentPost
            var post = action.payload;
            var posts = [...state.posts];
            var statuses = [...state.statuses];
            posts.forEach((p, i) => {
                if (p.id === post.id) {
                    posts[i] = post;
                }
            })
            statuses.forEach((s, sIndex) => {
                if (s.id === post.prevStatus) {
                    s.data.forEach((p, index) => {
                        if (p.id === post.id) {
                            // console.log('deleting')
                            s.data.splice(index, 1);
                        }
                    })
                }
            });
            // console.log(post, 'posss')
            statuses.forEach(s => {
                if (s.id === post.status) {
                    // console.log('adding', post)
                    s.data.push(post);
                }
            })
            return {
                ...state,
                posts: posts,
                statuses: statuses,
                currenPost: post
            }
        case PostActions.UPDATE_ATTACHMENT_SUCCESS:
            var attachment = action.payload;
            var post = { ...state.currentPost };
            post.loading = false;
            var attachments = [...post.attachments];
            attachments.forEach((p, index) => {
                if (p.id === attachment.id) {
                    attachments[index] = attachment;
                }
            })
            post.attachments = attachments;

            return {
                ...state,
                currentPost: post
            }


        case PostActions.UPDATE_COMMENT_SUCCESS:

            var post = { ...state.currentPost }
            var comment = action.payload;
            comment.loading = false;
            post.comments.forEach(c => {
                if (c.id === comment.id) {
                    c = comment
                }
            })

            return {
                ...state,
                currentPost: post
            }

        case PostActions.ADD_COMMENT_SUCCESS:
            var post = { ...state.currentPost }
            post.comments.push(action.payload);
            return {
                ...state,
                currentPost: post
            }

        case PostActions.UPDATE_COMMENT_ERROR:
            console.log('eroro')
            return {
                ...state,
                posts: [...state.posts]
            }

        case PostActions.DELETE_COMMENT_ERROR:
            return {
                ...state,
                posts: [...state.posts]
            }

        case PostActions.UPDATE_POST_SUCCESS:
            var index;
            var sIndex;
            var pIndex;
            var posts = [...state.posts];
            var topic = { ...state.currentTopic };
            var post = action.payload;
            // post.start = moment(post.date).set('hours', post.time.hours()).set('minutes', post.time.minutes())
            var statuses = [...state.statuses];
            // posts.forEach((p, i) => {
            //     if (p.id == post.id) {
            //         index = i;
            //     }
            // })
            // delete post.watchers;
            var updatePost = { ...state.currentPost, ...post, loading: false };
            // posts[index] = updatePost;
            // statuses.forEach((s, sI) => {
            //     s.data.forEach((p, pI) => {
            //         if (p.id === post.id) {
            //             sIndex = sI
            //             pIndex = pI;
            //         }
            //     })
            // })
            // statuses[sIndex].data[pIndex] = updatePost;
            // if (topic.topicPosts) {
            //     topic.topicPosts.forEach((p, i) => {
            //         if (p.id == updatePost.id) {
            //             topic.topicPosts[i] = updatePost;
            //         }
            //     })
            // }


            return {
                ...state,
                posts: posts,
                currentPost: updatePost,
                statuses: statuses,
                currentTopic: topic
            }

        case PostActions.UPDATE_TOPIC_SUCCESS:
            var topics = [...state.topics];
            var topic = { ...state.currentTopic, ...action.payload };
            topic.version = action.payload.version;

            topics.forEach((t, i) => {
                if (t.id === topic.id) {
                    topics.splice(i, 1)
                }
            })
            topics.push(topic);

            return {
                ...state,
                topics: topics,
                currentTopic: topic
            }

        case PostActions.DELETE_COMMENT_SUCCESS:
            var post = { ...state.currentPost }
            post.comments.splice(action.payload, 1);
            return {
                ...state,
                currentPost: post
            }

        case PostActions.UI_HTTP:
            return {
                ...state,
                uiState: {
                    http: { ...state.uiState.http, ...action.payload }
                }
            }


        case PostActions.DELETE_POST_SUCCESS:
            const id = action.payload
            var posts = state.posts.slice();
            var statuses = [...state.statuses];
            var topic = { ...state.currentTopic };
            posts.forEach((p, i) => {
                if (p.id === id) {
                    posts.splice(i, 1)
                }
            })

            statuses.forEach((s) => {
                s.data.forEach((p, i) => {
                    if (p.id === id) {
                        s.data.splice(i, 1)
                    }
                })
            })
            if (topic.topicPosts) {
                topic.topicPosts.forEach((p, i) => {
                    if (p.id == id) {
                        topic.topicPosts.splice(i, 1)
                    }
                })
            }

            return { ...state, posts: posts, statuses: statuses, currentTopic: topic };

        case PostActions.DELETE_TOPIC_SUCCESS:
            const topicId = action.payload
            var topics = state.topics.slice();

            topics.forEach((t, i) => {
                if (t.id === id) {
                    topics.splice(i, 1)
                }
            })

            return { ...state, topics: topics };



        case PostActions.SET_STATUSES:
            return { ...state, statuses: [...action.payload] };

        case PostActions.GET_POSTS_SUCCESS:
            return { ...state, posts: [...action.payload] }

        case PostActions.GET_POST_SUCCESS:
            return {
                ...state,
                currentPost: action.payload
            }
        case PostActions.GET_TOPIC_SUCCESS:
            var currentTopic = action.payload;
            var posts = [...state.posts];
            var currentTopicPosts = [];
            posts.forEach(p => {
                if (p.epics) {
                    p.epics.forEach(e => {
                        if (e.id == currentTopic.id) {
                            currentTopicPosts.push(p);
                        }
                    })
                }
            });

            currentTopic.topicPosts = currentTopicPosts;
            return {
                ...state,
                currentTopic: currentTopic
            }

        case PostActions.WATCH_POST_SUCCESS:
            var user = action.payload;
            var post = { ...state.currentPost };
            post.is_watcher = !post.is_watcher;
            if (post.is_watcher) {
                post.watchers.unshift(user);
            }
            else {
                post.watchers = post.watchers.filter(o => o.id !== user.id);
            }

            return {
                ...state, currentPost: post
            }
        case PostActions.ADD_ATTACHMENT_SUCCESS:

            var attachment = { ...action.payload };
            var post = { ...state.currentPost };
            post.attachments.push(attachment)
            return {
                ...state,
                currentPost: post
            }

        case PostActions.DELETE_ATTACHMENT_SUCCESS:

            var index = action.payload;
            var post = { ...state.currentPost };
            post.attachments.splice(index, 1);
            return {
                ...state,
                currentPost: post
            }

        case PostActions.WATCH_TOPIC_SUCCESS:
            var user = action.payload;
            var topic = { ...state.currentTopic };
            // console.log(user, 'user');
            topic.is_watcher = !topic.is_watcher;
            if (topic.is_watcher) {
                topic.watchers.unshift(user);
            }
            else {
                topic.watchers = topic.watchers.filter(o => o.id !== user.id);
            }

            return {
                ...state, currentTopic: topic
            }
        case PostActions.GET_COMMENTS_SUCCESS:
            var comments = action.payload;

            var post = { ...state.currentPost, comments: comments };


            return {
                ...state,
                currentPost: post
            }

        default:
            return {
                ...state
            }
    }


}