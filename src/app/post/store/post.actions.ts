import { Action } from '@ngrx/store';
export const CREATE_POST = 'CREATE_POST';
export const CREATE_POST_SUCCESS = 'CREATE_POST_SUCCESS';
export const CREATE_POST_ERROR = 'CREATE_POST_ERROR';
export const DRAG_POST = 'DRAG_POST';
export const DRAG_POST_SUCCESS = 'DRAG_POST_SUCCESS';
export const DRAG_POST_ERROR = 'DRAG_POST_ERROR';
export const DRAG_TOPIC = 'DRAG_TOPIC';
export const DRAG_TOPIC_SUCCESS = 'DRAG_TOPIC_SUCCESS';
export const DRAG_TOPIC_ERROR = 'DRAG_TOPIC_ERROR';
export const ADD_COMMENT = 'ADD_COMMENT';
export const ADD_COMMENT_SUCCESS = 'ADD_COMMENT_SUCCESS';
export const ADD_COMMENT_ERROR = 'ADD_COMMENT_ERROR';
export const DELETE_COMMENT = 'DELETE_COMMENT';
export const DELETE_COMMENT_SUCCESS = 'DELETE_COMMENT_SUCCESS';
export const DELETE_COMMENT_ERROR = 'DELETE_COMMENT_ERROR';
export const DELETE_TOPIC = 'DELETE_TOPIC';
export const DELETE_TOPIC_SUCCESS = 'DELETE_TOPIC_SUCCESS'
export const DELETE_TOPIC_ERROR = 'DELETE_TOPIC_ERROR'
export const DELETE_POST = 'DELETE_POST';
export const DELETE_POST_SUCCESS = 'DELETE_POST_SUCCESS'
export const DELETE_POST_ERROR = 'DELETE_POST_ERROR'
export const SET_POSTS = 'SET_POSTS';
export const SET_STATUSES = 'SET_STATUSES'
export const GET_POSTS = 'GET_POSTS';
export const GET_POSTS_SUCCESS = 'GET_POSTS_SUCCESS'
export const GET_POSTS_ERROR = 'GET_POSTS_ERROR'
export const GET_POST = 'GET_POST';
export const GET_POST_SUCCESS = 'GET_POST_SUCCESS';
export const GET_POST_ERROR = 'GET_POST_ERROR';
export const GET_TOPIC = 'GET_TOPIC';
export const GET_TOPIC_SUCCESS = 'GET_TOPIC_SUCCESS';
export const GET_TOPIC_ERROR = 'GET_TOPIC_ERROR';
export const CREATE_TOPIC = 'CREATE_TOPIC';
export const CREATE_TOPIC_SUCCESS = 'CREATE_TOPIC_SUCCESS';
export const CREATE_TOPIC_ERROR = 'CREATE_TOPIC_ERROR';
export const WATCH_POST_SUCCESS = 'WATCH_POST_SUCCESS';
export const WATCH_POST = 'WATCH_POST'
export const WATCH_POST_ERROR = 'WATCH_POST_ERROR';
export const WATCH_TOPIC = 'WATCH_TOPIC'
export const WATCH_TOPIC_SUCCESS = 'WATCH_TOPIC_SUCCESS'
export const WATCH_TOPIC_ERROR = 'WATCH_TOPIC_ERROR'
export const UPDATE_TOPIC = 'UPDATE_TOPIC'
export const UPDATE_TOPIC_SUCCESS = 'UPDATE_TOPIC_SUCCESS'
export const UPDATE_TOPIC_ERROR = 'UPDATE_TOPIC_ERROR'
export const UPDATE_POST_SUCCESS = 'UPDATE_POST_SUCCESS';
export const UPDATE_POST = 'UPDATE_POST';
export const UPDATE_POST_ERROR = 'UPDATE_POST_ERROR';
export const UPDATE_COMMENT = 'UPDATE_COMMENT';
export const UPDATE_COMMENT_SUCCESS = 'UPDATE_COMMENT_SUCCESS';
export const UPDATE_COMMENT_ERROR = 'UPDATE_COMMENT_ERROR';
export const UI_HTTP = 'UI_HTTP';
export const ADD_ATTACHMENT = 'ADD_ATTACHMENT';
export const ADD_ATTACHMENT_SUCCESS = 'ADD_ATTACHMENT_SUCCESS';
export const ADD_ATTACHMENT_ERROR = 'ADD_ATTACHMENT_ERROR';
export const DELETE_ATTACHMENT = 'DELETE_ATTACHMENT';
export const DELETE_ATTACHMENT_SUCCESS = 'DELETE_ATTACHMENT_SUCCESS'
export const DELETE_ATTACHMENT_ERROR = 'DELETE_ATTACHMENT_ERROR'
export const UPDATE_ATTACHMENT = 'UPDATE_ATTACHMENT';
export const UPDATE_ATTACHMENT_SUCCESS = 'UPDATE_ATTACHMENT_SUCCESS'
export const GET_COMMENTS='GET_COMMENTS'
export const GET_COMMENTS_SUCCESS='GET_COMMENTS_SUCCESS'
export const GET_COMMENTS_ERROR='GET_COMMENTS_ERROR'
export const READ_NOTIFICATION='READ_NOTIFICATION'

export class CreatePost implements Action {
    readonly type = CREATE_POST;
    constructor(public payload) { }
}

export class CreatePostSuccess implements Action {
    readonly type = CREATE_POST_SUCCESS;
    constructor(public payload) { }
}

export class DeletePost implements Action {
    readonly type = DELETE_POST;
    constructor(public payload) { }
}

export class DeletePostSuccess implements Action {
    readonly type = DELETE_POST_SUCCESS;
    constructor(public payload) { }
}

export class DeletePostError implements Action {
    readonly type = DELETE_POST_ERROR;
    constructor(public payload) { }
}

export class CreatePostError implements Action {
    readonly type = CREATE_POST_ERROR;
    constructor(public payload) { }
}

export class DragPost implements Action {
    readonly type = DRAG_POST;
    constructor(public payload) { }
}

export class DragPostSuccess implements Action {
    readonly type = DRAG_POST_SUCCESS;
    constructor(public payload) { }
}

export class DragPostError implements Action {
    readonly type = DRAG_POST_ERROR;
    constructor(public payload) { }
}

export class AddComment implements Action {
    readonly type = ADD_COMMENT;
    constructor(public payload) { }
}

export class AddCommentSuccess implements Action {
    readonly type = ADD_COMMENT_SUCCESS;
    constructor(public payload) { }
}


export class AddCommentError implements Action {
    readonly type = ADD_COMMENT_ERROR;
    constructor(public payload) { }
}

export class DeleteComment implements Action {
    readonly type = DELETE_COMMENT;
    constructor(public payload) { }
}

export class DeleteCommentSuccess implements Action {
    readonly type = DELETE_COMMENT_SUCCESS;
    constructor(public payload) { }
}

export class DeleteCommentError implements Action {
    readonly type = DELETE_COMMENT_ERROR;
}

export class DeleteTopic implements Action {
    readonly type = DELETE_TOPIC;
    constructor(public payload) { }
}

export class DeleteTopicSuccess implements Action {
    readonly type = DELETE_TOPIC_SUCCESS;
    constructor(public payload) { }
}

export class DeleteTopicError implements Action {
    readonly type = DELETE_TOPIC_ERROR;
    constructor(public payload) { }
}


export class CreateTopic implements Action {
    readonly type = CREATE_TOPIC;
    constructor(public payload) { }
}

export class CreateTopicError implements Action {
    readonly type = CREATE_TOPIC_ERROR;
    constructor(public payload) { }
}

export class CreateTopicSuccess implements Action {
    readonly type = CREATE_TOPIC_SUCCESS;
    constructor(public payload) { }
}

export class UpdateTopic implements Action {
    readonly type = UPDATE_TOPIC;
    constructor(public payload) { }
}

export class UpdateTopicError implements Action {
    readonly type = UPDATE_TOPIC_ERROR;
    constructor(public payload) { }
}


export class UpdateTopicSuccess implements Action {
    readonly type = UPDATE_TOPIC_SUCCESS;
    constructor(public payload) { }
}

export class ReadNotification implements Action {
    readonly type = READ_NOTIFICATION;
    constructor(public payload) { }
}


export class GetPosts implements Action {
    readonly type = GET_POSTS;
    constructor(public payload) { }
}

export class GetPostsError implements Action {
    readonly type = GET_POSTS_ERROR;
    constructor(public payload) { }
}

export class GetPostsSuccess implements Action {
    readonly type = GET_POSTS_SUCCESS;
    constructor(public payload) { }
}

export class GetPost implements Action {
    readonly type = GET_POST;
    constructor(public payload) { }
}

export class GetPostError implements Action {
    readonly type = GET_POST_ERROR;
    constructor(public payload) { }
}

export class GetPostSuccess implements Action {
    readonly type = GET_POST_SUCCESS;
    constructor(public payload) { }
}

export class GetTopic implements Action {
    readonly type = GET_TOPIC;
    constructor(public payload) { }
}

export class GetTopicError implements Action {
    readonly type = GET_TOPIC_ERROR;
    constructor(public payload) { }
}

export class GetTopicSuccess implements Action {
    readonly type = GET_TOPIC_SUCCESS;
    constructor(public payload) { }
}

export class SetPosts implements Action {
    readonly type = SET_POSTS;
    constructor(public payload) { }
}

export class SetStatuses implements Action {
    readonly type = SET_STATUSES;
    constructor(public payload) { }
}

export class WatchPost implements Action {
    readonly type = WATCH_POST;
    constructor(public payload) { }
}

export class WatchPostError implements Action {
    readonly type = WATCH_POST_ERROR;
    constructor(public payload) { }
}

export class WatchPostSuccess implements Action {
    readonly type = WATCH_POST_SUCCESS;
    constructor(public payload) { }
}

export class WatchTopic implements Action {
    readonly type = WATCH_TOPIC;
    constructor(public payload) { }
}

export class WatchTopicError implements Action {
    readonly type = WATCH_TOPIC_ERROR;
}

export class WatchTopicSuccess implements Action {
    readonly type = WATCH_TOPIC_SUCCESS;
    constructor(public payload) { }
}

export class AddAttachment implements Action {
    readonly type = ADD_ATTACHMENT;
    constructor(public payload) { }
}

export class AddAttachmentError implements Action {
    readonly type = ADD_ATTACHMENT_ERROR;
    constructor(public payload) { }
}

export class AddAttachmentSuccess implements Action {
    readonly type = ADD_ATTACHMENT_SUCCESS;
    constructor(public payload) { }
}

export class DeleteAttachment implements Action {
    readonly type = DELETE_ATTACHMENT;
    constructor(public payload) { }
}

export class DeleteAttachmentError implements Action {
    readonly type = DELETE_ATTACHMENT_ERROR;
    constructor(public payload) { }
}

export class DeleteAttachmentSuccess implements Action {
    readonly type = DELETE_ATTACHMENT_SUCCESS;
    constructor(public payload) { }
}


export class UpdatePost implements Action {
    readonly type = UPDATE_POST;
    constructor(public payload) { }
}

export class UpdatePostError implements Action {
    readonly type = UPDATE_POST_ERROR;
    constructor(public payload) { }
}


export class UpdatePostSuccess implements Action {
    readonly type = UPDATE_POST_SUCCESS;
    constructor(public payload) { }
}

export class UpdateComment implements Action {
    readonly type = UPDATE_COMMENT;
    constructor(public payload) { }
}

export class UpdateCommentError implements Action {
    readonly type = UPDATE_COMMENT_ERROR;
}

export class UpdateCommentSuccess implements Action {
    readonly type = UPDATE_COMMENT_SUCCESS;
    constructor(public payload) { }
}

export class UiHttp implements Action {
    readonly type = UI_HTTP;
    constructor(public payload) { }
}

export class UpdateAttachment implements Action {
    readonly type = UPDATE_ATTACHMENT;
    constructor(public payload) { }
}

export class UpdateAttachmentSuccess implements Action {
    readonly type = UPDATE_ATTACHMENT_SUCCESS;
    constructor(public payload) { }
}

export class GetComments implements Action {
    readonly type = GET_COMMENTS;
    constructor(public payload) { }
}

export class GetCommentsSuccess implements Action {
    readonly type = GET_COMMENTS_SUCCESS;
    constructor(public payload) { }
}

export class GetCommentsError implements Action {
    readonly type = GET_COMMENTS_ERROR;
    constructor(public payload) { }
}

export class DragTopic implements Action {
    readonly type = DRAG_TOPIC;
    constructor(public payload) { }
}

export class DragTopicSuccess implements Action {
    readonly type = DRAG_TOPIC_SUCCESS;
    constructor(public payload) { }
}
export class DragTopicError implements Action {
    readonly type = DRAG_TOPIC_ERROR;
    constructor(public payload) { }
}

export type PostActions =
    CreatePost |
    CreatePostSuccess |
    CreatePostError |
    DragPost |
    DragPostSuccess |
    DragPostError |
    AddComment |
    AddCommentSuccess |
    AddCommentError |
    DeleteComment |
    DeleteCommentSuccess |
    DeleteCommentError |
    DeleteTopic |
    DeleteTopicSuccess |
    DeleteTopicError |
    DeletePost |
    DeletePostSuccess |
    DeletePostError |
    SetPosts |
    SetStatuses |
    GetPosts |
    GetPostsSuccess |
    GetPostsError |
    GetTopic |
    GetTopicSuccess |
    GetTopicError |
    WatchPost |
    WatchPostSuccess |
    WatchPostError |
    WatchTopic |
    WatchTopicSuccess |
    WatchTopicError |
    GetPost |
    GetPostSuccess |
    GetPostError |
    UpdatePost |
    UpdatePostSuccess |
    UpdatePostError |
    UiHttp |
    UpdateComment |
    UpdateCommentSuccess |
    UpdateCommentError |
    UpdateTopic |
    UpdateTopicSuccess |
    UpdateTopicError |
    AddAttachment |
    AddAttachmentSuccess |
    AddAttachmentError |
    DeleteAttachment |
    DeleteAttachmentSuccess |
    DeleteAttachmentError |
    UpdateAttachment |
    UpdateAttachmentSuccess |
    GetComments |
    GetCommentsSuccess |
    GetCommentsError |
    ReadNotification |
    DragTopic |
    DragTopicSuccess |
    DragTopicError

