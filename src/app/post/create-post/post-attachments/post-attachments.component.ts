import { SharedService } from './../../../providers/shared.service';
import { PostService } from './../../post.service';
import { config } from './../../../providers/config';
import * as PostActions from './../../store/post.actions';
import { AppState } from './../../../store/app.reducers';
import { Component, OnInit, ChangeDetectionStrategy, ViewChild, ChangeDetectorRef } from '@angular/core';
import { Store, State } from '@ngrx/store';
import { ClipboardService } from '../../../../../node_modules/ngx-clipboard';
declare var swal: any; 

@Component({
  selector: 'post-attachments',
  templateUrl: './post-attachments.component.html',
  styleUrls: ['./post-attachments.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PostAttachmentsComponent implements OnInit {
  project;
  post;
  @ViewChild('uploadFile') uploadFile;
  uiState;
  images = config.images;

  constructor(private store: Store<AppState>,
    private detector: ChangeDetectorRef, private postService: PostService,
    private shared: SharedService,
    private _clipboardService: ClipboardService,
    private state: State<AppState>) { }


  ngOnInit() {
    this.project = this.state.value.main.project;
    this.store.select('post').subscribe(state => {
      this.post = state.currentPost;
      this.uiState = state.uiState;
      this.detector.markForCheck();
    });
  }

  uploadFiles() {
    this.uploadFile.nativeElement.click();
  }

  toggleVisibility(file) {
    file.loading = true;
    file.isVisible = !file.isVisible;
    console.log(file, 'file');
    this.store.dispatch(new PostActions.UpdateAttachment({ isVisible: file.isVisible, id: file.id }));
  }

  copyLink(file) {
    this._clipboardService.copyFromContent(file.url);
    this.shared.notify('Link Copied!', 'success');
    console.log(file, 'link');
  }


  selectFiles(event) {

    let file = event.target.files[0];

    swal({
      title: 'Are you sure?',
      text: 'You want to upload this file?',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, upload it!',
      reverseButtons: true
    })
      .then((result) => {
        console.log(result, 'result')
        if (result.value) {
          let formData = new FormData();
          formData.append('project', this.state.value.main.project.id.toString());
          formData.append('attached_file', file, file.name);
          formData.append('object_id', this.post.id);
          this.store.dispatch(new PostActions.AddAttachment(formData));
        }
      });
  }

  deleteFile(file, index) {
    swal({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!',
      reverseButtons: true
    }).then((result) => {
      swal.showLoading();
      if (result.value) {
        file.loading = true;
        this.store.dispatch(new PostActions.DeleteAttachment({ file: file, index: index }));
      }
    });
  }

}
