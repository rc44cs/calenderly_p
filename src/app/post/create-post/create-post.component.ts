import * as MainActions from './../../layout/store/main.actions';
import * as PostActions from './../store/post.actions';
import { Observable, Subscription } from 'rxjs';
import { AppState } from './../../store/app.reducers';
import { SharedService } from './../../providers/shared.service';
import { EventsService } from './../../providers/events.service';
import { Store, State } from '@ngrx/store';
import { Router, ActivatedRoute } from '@angular/router';
import { PostService } from './../post.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit, Input, Output, EventEmitter, ViewChild, ElementRef, OnChanges, OnDestroy } from '@angular/core';
import * as _ from 'underscore';
import { AutoUnsubscribe } from '../../layout/auto-unsub';
import * as moment from 'moment';
import { TourService } from '../../../../node_modules/ngx-tour-core';
import { PostModel } from './create-post.model'
declare var $: any;

@Component({
    selector: 'app-create-post',
    templateUrl: './create-post.component.html',
    styleUrls: ['./create-post.component.scss']
})
@AutoUnsubscribe
export class CreatePostComponent implements OnInit, OnChanges, OnDestroy {
    @Output() exit = new EventEmitter();
    @Input() date;
    @Input() status;
    @Input() time;
    @Input() postData;
    validateForm: PostModel = new PostModel();
    title: string;
    @ViewChild('editor') editor: ElementRef;
    @ViewChild('f') validator: any;
    @ViewChild('titlVar') titlVarValidator: any;
    @ViewChild('dateVar') datevarValidator: any;
    @ViewChild('timeVar') timeVarValidator: any;
    createPost: FormGroup;
    editMode = false;
    todayDate = new Date();
    topics = [];
    tags = [];
    preTopics = [];
    formattedTime = '';
    preloadProfiles = ['Facebook', 'Linked-In', 'Twitter'];
    profiles = [];
    deletedTopics = [];
    description;
    topic: any;
    showTimeDiv;
    tag = '';
    profile = '';
    subscriptions = [];
    subscription: Subscription;
    searchObservable: Observable<any>;
    uiState: any = {};
    isShow = 0;

    constructor(private fb: FormBuilder, private postService: PostService,
        private eventsService: EventsService, private sharedService: SharedService,
        private route: ActivatedRoute, public ts: TourService, private state: State<AppState>,
        private store: Store<AppState>,
        private router: Router
    ) { }



    ngOnInit() {
        // var isEditMode = isNaN(Number(location.pathname.split('/').pop()));
        this.initForm();
        this.searchObservable = Observable.create(observer => {
            observer.next([]);
            observer.complete();
        });

        this.subscriptions[0] = this.store.select('post').subscribe(state => {
            this.postData = state.currentPost;
        });
        this.subscriptions[0] = this.store.select('main').subscribe(state => {
            this.uiState = state.uiState;
        });

        this.subscriptions[1] = this.eventsService.hidePostSubject.subscribe(() => {
            this.createPost.reset();
            this.topics = [];
            this.tag = '';
            this.topic = '';
            this.profile = '';
            this.tags = [];
            this.profiles = [];
            this.createPost.markAsUntouched();
        });

        this.subscriptions[2] = this.postService.update$.subscribe(data => {
            this.description = data;
        });


        this.subscriptions[3] = this.route.params.subscribe(param => {
            this.editMode = param.id ? true : false;
            if (this.postData && this.editMode) {       // change after finding a bug to only this.topicData
                this.loadPost(this.postData);
            }
        });

        this.eventsService.currentData.subscribe(res => {
            if (res >= 0 ) {
                this.isShow = res;
            }
        });

    }



    submitForm() {
        let err = false;
        err = this.validatePost();
        if (!err && !this.editMode) {
            if (!this.profiles.length) {
                this.sharedService.notify('Please add a profile');
                return;
            }
            const w = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
            const postValue = {
                ...this.createPost.value,
                description: this.description,
                tags: this.tags,
                socialMedia: this.profiles,
                topics: this.topics,
                status: this.status,
                date: this.date,
                time: this.time
            };

            if (this.profiles.length && this.date && this.time) {
                this.store.dispatch(new PostActions.UiHttp({ topic: true }));
                this.store.dispatch(new PostActions.UiHttp({ calendar: true }));
                this.store.dispatch(new MainActions.ChangeUiState({
                    kanban: true
                }))
                console.log(postValue, 'formate time');
                this.store.dispatch(new PostActions.CreatePost(postValue));
                this.eventsService.hidePostSubject.next();
                this.description = undefined;
                this.date = undefined;
                this.time = undefined;
                this.formattedTime = undefined;
                if (w > 767 && this.uiState.showTour) {
                    // if (true) {
                    this.sharedService.notify('You have successfully completed the tour', 'success');
                    this.store.dispatch(new MainActions.ChangeUiState({
                        showTour: false
                    }));
                    this.ts.end();
                    this.ts.initialize([]);
                    const styleTag = $('<style>popover-container { display:none  !important;}</style>')
                    $('html > head').append(styleTag);
                }
            } else {
                this.sharedService.notify('Please enter valid information')
            }
        } else {
            this.sharedService.notify('Please enter valid information');
        }

    }
    updatePost() {
        if (!this.validatePost() && this.editMode) {
            var topics;
            var deletedTopics;

            if (!this.profiles.length) {
                this.sharedService.notify('Please add a profile');
                return;
            }
            this.postData.loading = true;
            topics = this.topics;
            deletedTopics = this.deletedTopics;
            var val = this.createPost.value;
            var post = {
                ...val,
                project: this.postData.project,
                version: this.postData.version,
                id: this.postData.id,
                tags: this.tags,
                topics: topics,
                socialMedia: this.profiles,
                date: this.date,
                time: this.time,
                deletedTopics: deletedTopics,
                description: this.description
            }
            this.store.dispatch(new PostActions.UpdatePost(post));
            this.deletedTopics = [];
        }
    }
    suggestTopics(e?: any) {
        if (this.subscription) {
            this.subscription.unsubscribe();
        }
        // console.log(e);
        this.searchObservable = this.sharedService.searchEpics(this.topic);
    }

    cancel() {
        if (this.validator) {
            this.validator.submitted = false;
            this.timeVarValidator.control.touched = false;
            this.datevarValidator.control.touched = false;
        }
        this.description = '';
        this.title = '';
        this.tags = [];
        this.topics = [];
        this.status = '';
        this.tag = '';
        this.profile = '';
        this.topic = '';
        this.eventsService.hidePostSubject.next(null);
    }


    private initForm() {
        this.createPost = this.fb.group({
            title: this.fb.control(null, [Validators.required])
        });
    }

    validatePost() {
        let err = false;
        if (!this.createPost.value.title) {
            this.validateForm.title = true;
            err = true;
        }
        if (!this.date) {
            this.validateForm.date = true;
            err = true;
        }

        if (!this.time) {
            this.validateForm.time = true;
            err = true;
        }
        return err;
    }

    changeTitle(event) {
        // this.createPost.value.title = event;
        this.validateForm.title = false;
    }

    changeDate() {
        this.validateForm.date = false;
    }



    addTopic(topic) {
        if (_.findIndex(this.topics, { id: topic.id }) === -1) {
            this.topics.push(topic);
            this.topic = '';
        }
    }

    addTag(tag) {
        if (!this.tag) {
            return false;
        }
        if (this.tags.indexOf(tag) === -1) {
            this.tags.push(tag);
            this.tag = '';
        }
    }

    addProfile(profile) {
        if (!this.profile) {
            return false;
        }
        if (this.profiles.indexOf(profile) === -1) {
            this.profiles.push(profile);
            this.profile = '';
        }
    }

    removeTopic(index) {
        this.deletedTopics.push(this.topics[index]);
        this.topics.splice(index, 1);
    }

    removeTag(index) {
        this.tags.splice(index, 1);
    }

    removeProfile(index) {
        this.profiles.splice(index, 1);
    }



    quillConfig = {
        toolbar: [
            ['bold', 'italic', 'underline'],        // toggled buttons
            [{ 'list': 'ordered' }, { 'list': 'bullet' }],
            [{ 'indent': '-1' }, { 'indent': '+1' }],          // outdent/indent
            [{ 'color': [] }, { 'background': [] }],          // dropdown with defaults from theme
            [{ 'align': [] }],
            ['clean'],                                         // remove formatting button
        ]
    };

    goback() {
        this.router.navigate(['/', 'calendar']);
    }

    loadPost(post: any) {
        this.editMode = true;
        this.createPost.patchValue(post);
        this.topics = post.topics;
        this.tags = post.tags;
        this.profiles = post.social_media_attributes;
        this.description = post.description;
    }

    ngOnDestroy() {
        this.createPost.reset();
        this.topics = [];
        this.profiles = [];
        this.postData.attachments = [];
        this.postData = null;
        this.tags = [];
        if (this.postData) {
            this.postData.loading = false;
        }
    }
    changeTime(e) {
        this.formattedTime = moment(e).format('h:mm a');
        this.validateForm.time = false;
    }

    ngOnChanges(change) {
        this.validateForm = new PostModel();

        if (change.date) {
            this.date = change.date.currentValue;
        }

        if (change.time) {
            this.time = change.time.currentValue;
            if (this.time) {
                this.formattedTime = moment(this.time).format('h:mm a');
            } else {
                this.formattedTime = null;
            }
        }

        if (change.status) {
            this.status = change.status.currentValue;
        }
    }

}
