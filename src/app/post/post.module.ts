import { CoreModule } from './../core/core.module';
import { ToggleViewPipe } from './post-view/post-comments/toggle-view.pipe';
import { PostWatchersComponent } from './post-view/post-watchers/post-watchers.component';
import { PostCommentsComponent } from './post-view/post-comments/post-comments.component';
import { PostService } from './post.service';
import { PostViewComponent } from './post-view/post-view.component';
import { CreatePostComponent } from './create-post/create-post.component';
import { NgModule } from '@angular/core';
import { PostAttachmentsComponent } from './create-post/post-attachments/post-attachments.component';
import { EditorModule } from '../editor/editor.module';
import { QuillModule } from 'ngx-quill';
import { NgxSpinnerModule } from 'ngx-spinner';

@NgModule({
  imports: [
    CoreModule,
    EditorModule,
    QuillModule,
    NgxSpinnerModule
  ],
  declarations: [
    CreatePostComponent,
    PostViewComponent,
    PostCommentsComponent,
    PostWatchersComponent,
    ToggleViewPipe,
    PostAttachmentsComponent
  ],
  exports: [
    CreatePostComponent,
    PostViewComponent],
  providers: [PostService]
})

export class PostModule { }
