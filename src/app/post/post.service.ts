import { environment } from './../../environments/environment';
import { of, Subject, Observable, forkJoin, empty } from 'rxjs';
import { AppState } from './../store/app.reducers';
import { Store, State } from '@ngrx/store';
import { SharedService } from './../providers/shared.service';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import * as _ from 'underscore';
import * as moment from 'moment';
import { isArray } from 'util';
import { catchError, mergeMap, map, switchMap } from 'rxjs/operators';
@Injectable()
export class PostService {
  posts: any[] = [];
  constructor(
    private http: HttpClient,
    private router: Router,
    private sharedService: SharedService,
    private store: Store<AppState>,
    private state: State<AppState>) {
  }
  update$: Subject<any> = new Subject<any>();

  createPost(data) {
    var topics = data.topics;
    var observables = [];
    var resPost;
    if (data.topics.length) {
      return this.http.post(environment.url + '/api/v1/userstories', data)
        .pipe(mergeMap((post: any) => {
          resPost = post;
          resPost.epics = topics;
          topics.forEach(topic => {
            observables.push(this.addTopicToPost(topic.id, post.id));
          })
          return forkJoin(observables).pipe(map(res => resPost));
        }))
    }
    else {
      return this.http.post(environment.url + '/api/v1/userstories', data)
    }
  }

  dragPost(id, data, prevStatus) {
    return this.http.patch(environment.url + '/api/v1/userstories/' + id, data)
      .pipe(mergeMap((post: any) => this.getPost(id)), map((p: any) => {
        p.prevStatus = prevStatus;
        return p;
      }));
  }


  dragTopic(id, data, prevStatus) {
    return this.http.patch(environment.url + '/api/v1/epics/' + id, data)
      .pipe(switchMap((post: any) => this.getPost(id)))
  }

  editComment(postId, comment) {
    return this.http.post(environment.url + `/api/v1/history/userstory/${postId}/edit_comment?id=${comment.id}`, comment)
      .pipe(map(() => comment));
  }

  createTopic(topic) {
    return this.http.post(environment.url + '/api/v1/epics', topic);
  }

  deleteComment(data) {
    return this.http.post(environment.url + `/api/v1/history/userstory/${data.postId}/delete_comment?id=${data.commentId}`, {})
      .pipe(map(res => data.index))
  }

  updatePost(data: any) {
    var observables = [];
    var resPost;
    var topics;
    console.log(data, 'data')
    if (data.topics) {
      topics = data.topics;
    }
    var id = data.id;
    delete data.id;
    if (!topics && !data.deletedTopics) {
      return this.http.patch(environment.url + '/api/v1/userstories/' + id, data)
        .pipe(switchMap(post => this.getPost(id)))
    }
    else {
      return this.http.patch(environment.url + '/api/v1/userstories/' + id, data)
        .pipe(switchMap(post => this.getPost(id)),
          switchMap((post: any) => {
            resPost = post
            if (topics) {
              topics.forEach(topic => {
                if (!topic.color)
                  observables.push(this.addTopicToPost(topic.id, post.id));
              })
            }

            if (data.deletedTopics) {
              data.deletedTopics.forEach(topic => {
                observables.push(this.deleteTopicFromPost(topic.id, post.id));
              })
            }

            if (observables.length) {
              return forkJoin(observables)
                .pipe(switchMap(res => this.getPost(id)));
            }
            else {
              return this.getPost(id);
            }
          }))
    }
  }

  updateTopic(topic) {
    return this.http.patch(environment.url + '/api/v1/epics/' + topic.id, topic)
      .pipe(switchMap(resTopic => {
        return this.getTopic(topic.ref);
      }))
  }

  updateAssignedTo(topic) {
    // return this.http.patch(environment.url + '/api/v1/epics/' + topic.id, topic)
    //   .pipe(switchMap(resTopic => {
    //     // return this.getTopic(topic.ref);
    //   }))
    var obj = { assigned_to: topic.assigned_to, version: topic.version };
    return this.http.patch(environment.url + '/api/v1/epics/' + topic.id, obj).pipe(map((res: any) => {
      return res;
    }));
  }

  getComments(postId) {
    return this.http.get(environment.url + '/api/v1/history/userstory/' + postId)
      .pipe(map((comments: any[]) => comments.filter(c => (c.delete_comment_date === null && c.comment))))
  }

  getCommentsOnTopic(topicId) {
    return this.http.get(environment.url + '/api/v1/history/epic/' + topicId)
      .pipe(map((comments: any[]) => comments.filter(c => c.delete_comment_date === null)))
  }


  createAttachment(data) {
    return this.http.post(environment.url + '/api/v1/userstories/attachments', data)

  }

  deleteAttachment(file, index) {
    return this.http.delete(environment.url + '/api/v1/userstories/attachments/' + file.id)
      .pipe(map(() => index))

  }

  deletePost(postId) {
    return this.http.delete(environment.url + '/api/v1/userstories/' + postId)
      .pipe(map(() => postId))

  }

  deleteTopic(topicId) {
    return this.http.delete(environment.url + '/api/v1/epics/' + topicId)
      .pipe(map(() => topicId))

  }

  getAttachments(post) {

    return this.http.get(environment.url + `/api/v1/userstories/attachments?object_id=${post.id}\&project=${post.project}`)
  }
  project;

  mapPostToCalendarly(post) {
    post.start = post.date;
    post.project = this.state.value.main.project.id;
    post.subject = post.title;
    post.assigned_to = this.state.value.main.user.id;
    post.publish_date = post.date
    post.publish_time = post.time
    post.description_html = post.description;
    post.social_media_attributes = post.socialMedia;
    post.epics = post.topics;
    return { ...post };
  }

  toggleWatchPost(post) {
    return this.http.post(environment.url + '/api/v1/userstories/' + post.id + '/' + (!post.is_watcher ? 'watch' : 'unwatch'), null)
      .pipe(map(res => this.state.value.main.user))

  }

  toggleWatchTopic(topic) {
    return this.http.post(environment.url + '/api/v1/epics/' + topic.id + '/' + (!topic.is_watcher ? 'watch' : 'unwatch'), null)
      .pipe(map(res => this.state.value.main.user))

  }

  mapPostResponse(postData: any) {
    if (isArray(postData)) {
      postData.forEach(post => {
        post.title = post.subject;
        post.time = moment(post.publish_time)
        post.start = moment(post.publish_date).set('hours', post.time.hours()).set('minutes', post.time.minutes())
        post.topics = post.epics ? post.epics : [];
        post.socialMedia = post.social_media_attributes;
        var tags = [];
        if (post.tags) {
          post.tags.forEach(t => {
            tags.push(..._.compact(t));
          })
          post.tags = tags;
        }
      })

    }
    else {
      postData.title = postData.subject;
      postData.date = moment(postData.publish_date)
      postData.time = moment(postData.publish_time)
      postData.start = moment(postData.publish_date).set('hours', postData.time.hours()).set('minutes', postData.time.minutes())
      postData.topics = postData.epics ? postData.epics : [];
      var tags = [];
      if (postData.tags) {
        postData.tags.forEach(t => {
          tags.push(..._.compact(t));
        })
        postData.tags = tags;
      }
    }
    return postData;
  }


  getPost(postId) {
    return this.http.get(environment.url + '/api/v1/userstories/' + postId)
      .pipe(map(post => {
        return this.mapPostResponse(post)
      }), catchError(er => of({ er: 1 })));
  }

  getPostByRef(ref) {
    return this.http.get(environment.url + `/api/v1/userstories/by_ref?ref=${ref}\&project=${this.state.value.main.project.id}`)
      .pipe(map(post => {
        return this.mapPostResponse(post)
      }), catchError(er => of({ er: 1 })));
  }

  getTopic(ref) {
    return this.http.get(environment.url + `/api/v1/epics/by_ref?ref=${ref}\&project=${this.state.value.main.project.id}`)
      .pipe(map(topic => {
        return this.mapPostResponse(topic)
      }))
  }

  getLocalPost(id) {
    return this.posts.filter(p => p.id === id)[0];
  }


  updateAttachment(data) {
    return this.http.patch(environment.url + '/api/v1/userstories/attachments/' + data.id, data)
      .pipe((file: any) => {
        file.isVisible = data.isVisible;
        return file;
      })
  }

  getPosts() {
    var state = this.state.value.main;
    // assigned_to=${state.user.id}
    return this.http.get(environment.url + `/api/v1/userstories?&project=${state.project.id}`)
      .pipe(map((posts: any[]) => {
        this.posts = this.mapPostResponse(posts);
        return this.posts;
      }))

  } // confirm from miguel

  addTopicToPost(topic, post) {
    return this.http.post(environment.url + `/api/v1/epics/${topic}/related_userstories`, { epic: topic, user_story: post })
  }

  deleteTopicFromPost(topic, post) {
    return this.http.delete(environment.url + `/api/v1/epics/${topic}/related_userstories/${post}`)
  }

  getRelatedUserStories(topicId) {
    return this.http.get(environment.url + `/api/v1/epics/${topicId}/related_userstories`)
  }

  ngOnInit() {
    this.project = this.state.value.main.project;
  }

}
