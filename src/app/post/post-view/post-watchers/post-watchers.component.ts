import { Observable } from 'rxjs';
import { AppState } from './../../../store/app.reducers';
import { config } from './../../../providers/config';
import { Component, OnInit, Input, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { Store } from '@ngrx/store';
import { AutoUnsubscribe } from '../../../layout/auto-unsub';
@Component({
  selector: 'post-watchers',
  templateUrl: './post-watchers.component.html',
  styleUrls: ['./post-watchers.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
@AutoUnsubscribe
export class PostWatchersComponent implements OnInit {

  constructor(private store: Store<AppState>,private changeDetector:ChangeDetectorRef) { }
  fallbackImg = config.images.fallbackImg;
  watchers=[];
  subscriptions=[];
  ngOnInit() {
    this.subscriptions[0]= this.store.select('post').select('currentPost').subscribe(currentPost=>{
      this.watchers=currentPost.watchers;
      this.changeDetector.markForCheck();
    })
  }

  ngOnDestroy(){}


}
