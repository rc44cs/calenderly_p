import * as PostActions from './../store/post.actions';
import { AppState } from './../../store/app.reducers';
import { SharedService } from './../../providers/shared.service';
import { config } from './../../providers/config';
import { Store } from '@ngrx/store';
import { PostService } from './../post.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { AutoUnsubscribe } from '../../layout/auto-unsub';
declare var swal: any;
@Component({
  selector: 'app-post-view',
  templateUrl: './post-view.component.html',
  styleUrls: ['./post-view.component.scss']
})
@AutoUnsubscribe
export class PostViewComponent {
  images = config.images;
  constructor(private route: ActivatedRoute, private router: Router,
    private postService: PostService,
    private sharedService: SharedService,
    private store: Store<AppState>) {
    this.store.select('main').subscribe(state => {
      this.members = state.project.members;
    });
  }
  post: any = { comments: [], watchers: [] };
  date;
  time;
  watchers = [];
  user: any;
  uiState: any = {};
  subscriptions = [];
  statuses = [];
  members = []

  ngOnInit() {
    this.subscriptions[0] = this.store.select('post').subscribe(state => {
      this.uiState = state.uiState;
      this.post = state.currentPost;
      // this.status = this.post.status
    })

    this.subscriptions[1] = this.route.data.subscribe(data => {
      this.watchers = data.post.watchers;
      this.post = data.post.post;
      this.date = this.post.date.toDate();
      this.time = this.post.time.toDate();
    })
    this.subscriptions[2] = this.store.select('post').subscribe((state) => {
      this.statuses = state.statuses;
    })
  }
  status;
  changeStatus(e) {
    console.log(e, 'e')
    this.store.dispatch(new PostActions.UiHttp({
      watch: true
    }));
    this.store.dispatch(new PostActions.DragPost({
      id: this.post.id,
      prevStatus: this.post.status,
      data: {
        id: this.post.id,
        status: e,
        version: this.post.version
      }
    }));
  }

  onChangeAssignedTo(event) {
    var val = this.post.value;
    var post = {
      ...val,
      project: this.post.project,
      version: this.post.version,
      id: this.post.id,
      assigned_to: event
    }
    this.store.dispatch(new PostActions.UpdatePost(post));
  }

  watchPost() {
    this.store.dispatch(new PostActions.UiHttp({
      watch: true
    }));
    this.store.dispatch(new PostActions.WatchPost(this.post))
  }

  deletePost(post) {
    swal({
      title: 'Are you sure?',
      text: "You will not be able to recover this post",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Delete',
      reverseButtons: true
    }).then((result) => {
      if (result.value) {
        this.store.dispatch(new PostActions.UiHttp({ calendar: true }));
        this.store.dispatch(new PostActions.DeletePost(post.id));
        this.router.navigate(['/', 'calendar'])
      }
    })
  }
  ngOnDestroy() { }
}