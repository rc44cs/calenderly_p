import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'toggleView'
})
export class ToggleViewPipe implements PipeTransform {

  transform(value: any, isViewLess?: any): any {
    if (value.length > 2 && isViewLess) {
      return [value[0],value[1]];
    }
    else {
      return value;
    }
  }

}
