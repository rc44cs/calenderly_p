import * as MainActions from './../../../layout/store/main.actions';
import * as PostActions from './../../store/post.actions';
import { Component, OnInit, Input, Output, EventEmitter, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { config } from '../../../providers/config';
import { AppState } from '../../../store/app.reducers';
import { Store } from '@ngrx/store';
declare var swal:any;

@Component({
  selector: 'post-comments',
  templateUrl: './post-comments.component.html',
  styleUrls: ['./post-comments.component.scss'],
  changeDetection:ChangeDetectionStrategy.OnPush
})
export class PostCommentsComponent implements OnInit {

  constructor(private store:Store<AppState>,private ref:ChangeDetectorRef) { }
  @Input() comments;
  @Input() post;
  commentBody;
  @Input() user;
  fallbackImg=config.images.fallbackImg;
  isViewLess=true;
  uiState:any;

  ngOnInit() {
    this.store.select('post').subscribe(state=>{
      // console.log(state,'state')
      this.comments=state.currentPost.comments;
      this.uiState=state.uiState;
      this.ref.markForCheck();
    })

    this.store.select('main').subscribe(state=>{
      // console.log(state,'state')
      this.user=state.user;
    })
  }

  toggleComments()
  {
   this.isViewLess=!this.isViewLess;
  }

  submitComment(comment)
  {
    // console.log(comment,'comment');
    if(comment)
    {
      this.store.dispatch(new PostActions.UiHttp({
        comment:true
      }))
      this.store.dispatch(new PostActions.AddComment({comment:comment,version:1,id:this.post.id}));
      this.commentBody=''
    }
  }

  editComment(post,comment)
  {
    comment.loading=true;
    this.store.dispatch(new PostActions.UpdateComment({postId:post.id,comment:comment}));
    comment.isEditMode=false;
  }

  hideComment(comment)
  {
    comment.isEditMode=false;
  }

  removeComment(comment,i)
  {
    swal({
      title: 'Are you sure?',
      text: "You will not be able to recover this post",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Delete',
      reverseButtons: true
    }).then((result) => {
      if (result.value) {
        console.log(comment,'index');
        comment.loading=true;
        this.store.dispatch(new PostActions.DeleteComment({postId:this.post.id,commentId:comment.id,index:i}));
      }
    })
  }

}
