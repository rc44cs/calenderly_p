import { of, Observable, forkJoin, empty } from 'rxjs';
import * as MainActions from './../../layout/store/main.actions';
import * as PostActions from './../store/post.actions';
import { AppState } from './../../store/app.reducers';
import { Store } from '@ngrx/store';
import { SharedService } from '../../providers/shared.service';
import { PostService } from '../../post/post.service';
import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Resolve, Router } from '@angular/router';
import { mergeMap, map, catchError, finalize } from 'rxjs/operators';

@Injectable()
export class PostViewResolveGuard implements Resolve<any> {
  constructor(private postService: PostService, private sharedService: SharedService,
    private store: Store<AppState>, private router: Router) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> {
    this.store.dispatch(new PostActions.UiHttp({ calendar: true }));
    var observables = [];
    var watchers = [];
    var refId = route.params.id;
    var post;
    var comments;
    return this.postService.getPostByRef(refId)
      .pipe(mergeMap((p: any) => {
        post = p;
        if (p.watchers) {
          p.watchers.forEach(id => {
            observables.push(this.sharedService.getUserDetails(id))
          })
        }
        if (observables.length) {
          return forkJoin(observables);
        }
        else {
          return Observable.create(observer => {
            observer.next([]);
            observer.complete();
          })
        }
      }), mergeMap((results: any[]) => {
        console.log(post.id, 'postid');
        watchers = results;
        return this.postService.getComments(post.id)
      }), mergeMap((c) => {
        comments = c;
        return this.postService.getAttachments(post)
      }), map(attachments => {
        var newPost = {
          ...post,
          comments: comments,
          watchers: watchers,
          attachments: attachments
        }
        this.store.dispatch(new PostActions.GetPostSuccess(newPost));
        return {
          post: post,
          comments: comments,
          watchers: watchers
        }
      }), catchError(er => {
        console.log(er);
        this.router.navigate(['/', 'calendar']);
        return empty()
      }), finalize(() => {
        this.store.dispatch(new PostActions.UiHttp({ calendar: false }));
        this.store.dispatch(new MainActions.ChangeUiState({
          kanban: false
        }))
        this.store.dispatch(new PostActions.UiHttp({ topic: false }));
      }))
  }
}