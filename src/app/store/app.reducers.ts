import { ActionReducerMap } from "@ngrx/store";
import * as MainReducers from "../layout/store/main.reducers";
import * as PostReducer from '../post/store/post.reducer';

export interface AppState{
    post:PostReducer.State,
    main:MainReducers.State
}

export var AppReducers:ActionReducerMap<AppState>={
post:PostReducer.PostReducer,
main:MainReducers.MainReducers
}