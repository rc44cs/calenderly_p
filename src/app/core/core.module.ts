import { environment } from './../../environments/environment';
import { CalendarlyInterceptor } from './../providers/interceptors/calendarly-interceptor.service';
import { SpinnerComponent } from './../spinner/spinner.component';
import { TimepickerModule } from 'ngx-bootstrap/timepicker';
import { TypeaheadModule } from 'ngx-bootstrap/typeahead';
import { DragulaModule } from 'ng2-dragula';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { ModalModule } from 'ngx-bootstrap/modal';
import { AuthInterceptor } from './../providers/interceptors/auth-interceptor.service';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TooltipModule, ButtonsModule, BsDatepickerModule } from 'ngx-bootstrap';
import { ServiceWorkerService } from '../providers/service-worker.service';
import { ImgFallbackModule } from 'ngx-img-fallback';
import { Ng2IziToastModule } from 'ng2-izitoast';//<-- this line
import { NgxMaterialTimepickerModule } from 'ngx-material-timepicker';
import { TourNgxBootstrapModule } from 'ngx-tour-ngx-bootstrap';
import { RouterModule } from '@angular/router';
import { ClickOutsideModule } from 'ng-click-outside';
import { NgPipesModule } from 'ngx-pipes';
import { ClipboardModule } from 'ngx-clipboard';


@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    ModalModule.forRoot(),
    FormsModule,
    ReactiveFormsModule,
    BsDropdownModule.forRoot(),
    DragulaModule,
    TooltipModule.forRoot(),
    TypeaheadModule.forRoot(),
    ButtonsModule.forRoot(),
    BsDatepickerModule.forRoot(),
    TimepickerModule.forRoot(),
    ClipboardModule,
    ImgFallbackModule,
    Ng2IziToastModule,
    NgxMaterialTimepickerModule.forRoot(),
    TourNgxBootstrapModule.forRoot(),
    RouterModule,
    ClickOutsideModule,
    NgPipesModule
  ],
  declarations: [SpinnerComponent],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true,
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: CalendarlyInterceptor,
      multi: true,
    },
    environment.production ? ServiceWorkerService : []
  ],
  exports: [
    HttpClientModule,
    ModalModule,
    BsDropdownModule,
    DragulaModule,
    FormsModule,
    ReactiveFormsModule,
    BsDatepickerModule,
    TimepickerModule,
    CommonModule,
    BsDropdownModule,
    TooltipModule,
    ClipboardModule,
    TypeaheadModule,
    SpinnerComponent,
    ButtonsModule,
    ImgFallbackModule,
    Ng2IziToastModule,
    NgxMaterialTimepickerModule,
    TourNgxBootstrapModule,
    RouterModule,
    ClickOutsideModule,
    NgPipesModule
  ],
})
export class CoreModule { }
