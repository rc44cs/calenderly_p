export const environment = {
    production: true,
    appId: 'cac6ad54-c3d2-11e8-86a5-0284c3989b56',
    url: 'https://dev.taiga.calendar.ly',
    plan: {
        "id": 1,
        // "modified_date": "2018-10-17T13:43:23.087Z",
        // "user_limit": 50,
        // "storage_size": 3221225472,
        // "name": "Trial Closed Beta",
        // "created_date": "2018-10-17T13:43:23.084Z",
        // "duration": 186
    },
    scripts: [
        `    <script>
        (function(w, d, s, l, i) {
            w[l] = w[l] || [];
            w[l].push({
                'gtm.start': new Date().getTime(),
                event: 'gtm.js'
            });
            var f = d.getElementsByTagName(s)[0],
                j = d.createElement(s),
                dl = l != 'dataLayer' ? '&l=' + l : '';
            j.async = true;
            j.src = 'https://googletagmanager.com/gtm.js?id=' + i + dl + '&gtm_auth=F0Zn8x_LArB3F5hS5SCkGA&gtm_preview=env-6&gtm_cookies_win=x';
            f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-TCLHV5Q');
    </script>`,
        `<noscript><iframe src="https://googletagmanager.com/ns.html?id=GTM-TCLHV5Q&gtm_auth=F0Zn8x_LArB3F5hS5SCkGA&gtm_preview=env-6&gtm_cookies_win=x" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>`
    ]};

