export const environment = {
    production: true,
    appId: 'bf60d098-d3b8-11e8-b610-0298db54dd5a',
    url: 'https://prd.taiga.calendar.ly',
    plan: {
        "id": 2,
        // "name": "Trial Closed Beta",
        // "duration": 186,
        // "user_limit": 50,
        // "storage_size": 3221225472,
        // "created_date": "2018-10-17T13:43:44.809Z",
        // "modified_date": "2018-10-17T13:43:44.811Z"
    },
    scripts: [
        `    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl+ '&gtm_auth=RiSompYkYyZK0pKQXeXCYw&gtm_preview=env-7&gtm_cookies_win=x';f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-TCLHV5Q');</script>`,
        `<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TCLHV5Q&gtm_auth=RiSompYkYyZK0pKQXeXCYw&gtm_preview=env-7&gtm_cookies_win=x"
        height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>`
    ]};
